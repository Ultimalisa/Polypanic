
a_idle_C_yellow.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
yellow_arm_back
  rotate: false
  xy: 767, 781
  size: 234, 215
  orig: 234, 215
  offset: 0, 0
  index: -1
yellow_arm_front
  rotate: false
  xy: 577, 634
  size: 188, 362
  orig: 188, 362
  offset: 0, 0
  index: -1
yellow_body
  rotate: false
  xy: 2, 2
  size: 573, 994
  orig: 573, 994
  offset: 0, 0
  index: -1
