
a_sad_C_blue.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
blue_arm_back
  rotate: false
  xy: 783, 69
  size: 234, 215
  orig: 234, 215
  offset: 0, 0
  index: -1
blue_arm_front
  rotate: true
  xy: 419, 96
  size: 188, 362
  orig: 188, 362
  offset: 0, 0
  index: -1
blue_sad_body
  rotate: false
  xy: 2, 2
  size: 415, 846
  orig: 415, 846
  offset: 0, 0
  index: -1
blue_sad_head
  rotate: false
  xy: 419, 286
  size: 580, 562
  orig: 580, 562
  offset: 0, 0
  index: -1
