
a_win_C_red.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
red_arm_back
  rotate: false
  xy: 767, 781
  size: 234, 215
  orig: 234, 215
  offset: 0, 0
  index: -1
red_arm_front
  rotate: false
  xy: 577, 634
  size: 188, 362
  orig: 188, 362
  offset: 0, 0
  index: -1
red_body
  rotate: false
  xy: 2, 2
  size: 573, 994
  orig: 573, 994
  offset: 0, 0
  index: -1
