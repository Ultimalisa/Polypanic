
a_fire_item_idle_I.png
size: 512,128
format: RGBA8888
filter: Linear,Linear
repeat: none
fire_centre
  rotate: true
  xy: 2, 2
  size: 109, 177
  orig: 109, 177
  offset: 0, 0
  index: -1
fire_right
  rotate: true
  xy: 181, 41
  size: 70, 133
  orig: 70, 133
  offset: 0, 0
  index: -1
fire_small_left
  rotate: false
  xy: 316, 57
  size: 18, 54
  orig: 18, 54
  offset: 0, 0
  index: -1
fire_small_middle
  rotate: true
  xy: 181, 13
  size: 26, 75
  orig: 26, 75
  offset: 0, 0
  index: -1
