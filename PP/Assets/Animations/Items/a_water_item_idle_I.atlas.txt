
a_water_item_idle_I.png
size: 512,128
format: RGBA8888
filter: Linear,Linear
repeat: none
water_centre
  rotate: false
  xy: 299, 35
  size: 49, 55
  orig: 49, 55
  offset: 0, 0
  index: -1
water_left
  rotate: true
  xy: 2, 2
  size: 88, 175
  orig: 88, 175
  offset: 0, 0
  index: -1
water_right
  rotate: true
  xy: 179, 21
  size: 69, 118
  orig: 69, 118
  offset: 0, 0
  index: -1
