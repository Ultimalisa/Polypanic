
a_water_rainbow_W.png
size: 512,32
format: RGBA8888
filter: Linear,Linear
repeat: none
water_shot_base_rainbow_3
  rotate: false
  xy: 2, 2
  size: 416, 9
  orig: 416, 9
  offset: 0, 0
  index: -1
water_shot_front
  rotate: false
  xy: 423, 2
  size: 23, 20
  orig: 23, 20
  offset: 0, 0
  index: -1
water_shot_reflect_rainbow_3
  rotate: false
  xy: 2, 13
  size: 419, 9
  orig: 419, 9
  offset: 0, 0
  index: -1
