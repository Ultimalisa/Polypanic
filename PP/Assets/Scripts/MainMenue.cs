﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

public class MainMenue : MonoBehaviour {

    private Player m_playerOne;
    public Canvas m_mainCanvas;
    private BackToMainMenu m_backToMainMenu;

    private void Awake()
    {
        m_playerOne = ReInput.players.GetPlayer(0);
        m_backToMainMenu = GetComponentInChildren<BackToMainMenu>();
    }

    public void PlayGame()
    {
        GameManager.instance.m_bIsRunning = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex +1);
    }

    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
        
    }

    private void Update()
    {
        if (m_playerOne.GetButtonDown("Cancel"))
        {
            m_backToMainMenu.BackToMenu();
        }
    }
}
