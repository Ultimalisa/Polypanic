﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSpawnNew : MonoBehaviour {

    internal int m_playerSpawnNumber;
    internal int m_maxPlayerNumber = 4;
    internal AddMe m_addMe;
    // Use this for initialization
    void Start()
    {
        m_playerSpawnNumber = GameManager.instance.GetPlayerNumber();
        m_addMe = FindObjectOfType<AddMe>();
        m_addMe.Add();
        SpawnPlayers(m_playerSpawnNumber);
    }

   void SpawnPlayers(int _playerSpawnNumber)
    {

        for (int i = 0; i < m_maxPlayerNumber; i++)
        {
            if (i > _playerSpawnNumber-1)
            {
                Destroy(transform.GetChild(i).GetComponent<PlayerController>().m_healthBarCanvas.gameObject);
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        switch (_playerSpawnNumber)
        {
            case (2):
                GameManager.instance.m_playerUIAbove.transform.GetChild(2).gameObject.SetActive(false);
                GameManager.instance.m_playerUIAbove.transform.GetChild(3).gameObject.SetActive(false);
                break;
            case (3):
                GameManager.instance.m_playerUIAbove.transform.GetChild(2).gameObject.SetActive(false);
                break;
        }

        //switch (_playerSpawnNumber)
        //{
        //    case (2):
        //        transform.GetChild(2).gameObject.SetActive(false);
        //        //transform.GetChild(2).gameObject.GetComponent<Health>().m_playerHealth = 0;
        //        //transform.GetChild(2).gameObject.GetComponent<Health>().heart.fillAmount = 0;
        //        //transform.GetChild(2).gameObject.GetComponent<Health>().PlayerDeath();
        //
        //       transform.GetChild(3).gameObject.SetActive(false);
        //        //transform.GetChild(3).gameObject.GetComponent<Health>().m_playerHealth = 0;
        //        //transform.GetChild(3).gameObject.GetComponent<Health>().heart.fillAmount = 0;
        //        //transform.GetChild(3).gameObject.GetComponent<Health>().PlayerDeath();
        //        break;
        //    case (3):
        //        transform.GetChild(3).gameObject.SetActive(false);
        //        //transform.GetChild(3).gameObject.GetComponent<Health>().m_playerHealth = 0;
        //        //transform.GetChild(3).gameObject.GetComponent<Health>().heart.fillAmount = 0;
        //        //transform.GetChild(3).gameObject.GetComponent<Health>().PlayerDeath();
        //        break;
        //    case (4):
        //        Debug.Log("all players spawned");
        //        break;
        //    default:
        //        Debug.Log("BUG!!!!!!!!!!");
        //        break;
        //
        //}
    }
}
