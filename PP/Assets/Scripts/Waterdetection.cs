﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waterdetection : MonoBehaviour {

    float timer;
    public EPlayerID bulletID;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }


    private void OnTriggerStay2D (Collider2D collision)
    {
        
        Debug.Log(collision.gameObject.tag);

        if ((collision.gameObject.tag == "player") && (bulletID != collision.gameObject.GetComponent<PlayerController>().m_playerID&& collision.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable== false))
        {
            if (timer >= 0.3f)
            {
                collision.gameObject.GetComponent<Health>().Damage(1);
                collision.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable = true;
            }
        }
        
    }
    
}
