﻿using UnityEngine;

public class ArenaRoll : MonoBehaviour {

    public float Speed;
    public float ChangeAngle;
    public Vector3 RotVec;
    Vector3 StartVec;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Angle(StartVec, transform.forward) >= ChangeAngle)
        {
            StartVec = transform.forward;
            RotVec = -RotVec;
        }

        transform.Rotate(RotVec * Speed * Time.deltaTime);
    }


    void Awake()
    {
        StartVec = transform.forward;
    }

}
