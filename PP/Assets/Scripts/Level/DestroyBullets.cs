﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBullets : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter2D(Collider2D _collision)

    {
        if (_collision.gameObject.tag == "bullet")
        {
            Destroy(_collision.gameObject);
        }
    }
}
