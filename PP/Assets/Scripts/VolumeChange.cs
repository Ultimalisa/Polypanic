﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VolumeChange : MonoBehaviour {

    public Slider Volume;
    public AudioSource myMusic;
   public float timer = 0;
   public float secondTimer;
    bool neverDone = true;


    public void Awake()
    {
        
        DontDestroyOnLoad(this);
        
        secondTimer = timer;

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
        if (GameManager.instance.m_musicManager == null)
            GameManager.instance.m_musicManager = this;

        myMusic = GameManager.instance.m_musicManager.myMusic; 
    }

    // Update is called once per frame
    void Update () {

        if (Volume != null)
        {
            myMusic.volume = Volume.value;
            Volume.value = myMusic.volume;
        }
        
        timer += Time.deltaTime;

        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;


        if (sceneName == "Loris")
        {
            if (neverDone == true)
            {
                timer = 0;
                neverDone = false;
            }
            
          
            if ((int)timer == 6 + (int)secondTimer)
            {

                secondTimer = timer;
              //  myMusic.pitch += 0.01f;
            }
        }
    }
    float TimerUpdate()
    {
       return timer += Time.deltaTime;
    }
}
