﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class Health : MonoBehaviour
{
    [SerializeField]
    internal float m_playerHealth = 3f;
    internal float m_playerHealthMax;
    internal int m_damageCounter;

    public AudioSource m_deathSound;
    public AudioSource m_damageSound;

    internal Image m_healthbar;

    internal EPlayerID m_playerID;

    float m_timer;
    // Use this for initialization
    void Start()
    {
        m_playerID = GetComponent<PlayerController>().m_playerID;
        m_damageCounter = 0;
        m_playerHealthMax = m_playerHealth;
        m_healthbar = GetComponent<PlayerController>().m_healthBarCanvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        m_timer = m_timer - Time.deltaTime;
    }

   public  void Damage(int WeaponDamage)
   {
       
        // Set vibration for a certain duration
        foreach (Joystick j in gameObject.GetComponent<PlayerController>().m_player.controllers.Joysticks)
        {
            if (!j.supportsVibration) continue;

          
            
                if (j.vibrationMotorCount > 0) j.SetVibration(0, 0.8f, 0.7f); // 1 second duration
            
        }

        m_damageCounter++;
        m_damageSound.PlayScheduled(0.5);
        m_playerHealth = m_playerHealth - WeaponDamage;
        m_healthbar.fillAmount = (m_playerHealth/m_playerHealthMax );
        int ding =(int) m_playerHealthMax - m_damageCounter;
        if (ding == 1)
        {
            m_healthbar.GetComponent<Image>().color = new Color32(230, 0, 0, 255);
        }
        if (m_playerHealth <= 0)
        {
            PlayerDeath();
        }
   }
   public void PlayerDeath()
   {
        foreach (Joystick j in gameObject.GetComponent<PlayerController>().m_player.controllers.Joysticks)
        {
            if (!j.supportsVibration) continue;



            if (j.vibrationMotorCount > 0) j.SetVibration(0, 1.0f, 1.2f); // 1 second duration

        }


        m_deathSound.PlayScheduled(0.5);
        Destroy(GetComponent<PlayerController>().m_healthBarCanvas.gameObject);
        FindObjectOfType<Respawn>().m_playerAlive--;
        if (GetComponent<PlayerController>().m_bHasCoin)
        {
            GetComponent<PlayerController>().m_bHasCoin = false;
            GameObject coin = GameObject.Instantiate(GameManager.instance.m_coin, transform.position, transform.rotation, null);
        }
        gameObject.SetActive(false);
        
   }



}
          
