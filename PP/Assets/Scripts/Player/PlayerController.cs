﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.UI;

public enum EPlayerID
{
    Player1,
    Player2,
    Player3,
    Player4
}

public class TimerClass
{
    public float m_reload;
    public bool m_bReload;
}

public class PlayerController : MonoBehaviour {

    internal int m_lightLevel = 1;
    internal int m_fireLevel = 1;
    internal int m_waterLevel = 1;
    


    public static bool GameIsPaused = false;
    public GameObject optionsPausemenuUI;
    public GameObject pauseMenuUI;
    internal GameObject optionsCheck;

    //Enums
    public EPlayerID m_playerID;
    
    public EWeaponType m_weaponType;
    public EWeaponLevel m_weaponLevel;

    //Boolean
    internal bool m_bCanShoot;
    internal bool m_bSpecialShoot;
    internal bool m_bIsInvulnerable;
    internal bool m_bPlayerHoldsButton;
    internal bool m_bSpecialReload;
    [SerializeField]    //Debug
    internal bool m_bHasCoin;
    internal bool m_bStartCountdownRunning;
    internal bool m_bDirection;

    //Timer
    internal float m_invulCooldown;
    [SerializeField]
    internal float m_invulCooldownMax;
    internal float m_flashTimer;
    [SerializeField]
    internal float m_flashTimerMax;
    internal float m_holdTimer;
    [SerializeField]
    internal float m_holdTimeMax = 4.0f;
    [SerializeField]
    internal float m_reloadMax = 5f;
    internal float m_specialReload;
    internal float m_specialReloadMax = 1;
    internal float m_normalShotRecovery;
    [SerializeField]
    internal float m_normalShotRecoveryMax = 5f;

    //Additional Components
    public Transform m_firePoint;
    public Transform m_arm;
    internal Player m_player;
    internal Vector2 m_moveVector;
    internal Vector2 m_shootingDir;
    internal LineRenderer m_lineRenderer;
    internal float m_standardRange;
    [SerializeField]
    internal float m_standardRangeOrigin;
    [SerializeField]
    internal float m_standardSpeedOrigin;
    internal float m_standardSpeed;
    [SerializeField]
    internal float m_standardRecoilOrigin;
    internal float m_standardRecoil;
    internal Image m_specialPic;
    internal Timer m_gameTimer;
    [SerializeField]
    internal Canvas m_healthBarCanvas;
    [SerializeField]
    internal float m_playerCanvasOffset = 60f;
    [SerializeField]
    internal Text m_rangeCountText;
    [SerializeField]
    internal Text m_speedCountText;

    //Counter
    internal float m_speedPlusCounter;
    internal float m_rangePlusCounter;


    //Lists
    public SpriteRenderer[] m_spriteRendererArray;
    public List<SpriteRenderer> m_spriteRendererList;
    public List<ParticleSystem> m_particlesystems;

    // Use this for initialization
    void Awake () {

        GameManager.instance.m_playerControllers.Add(this);

        //Sort players in PlayerController List of GameManager
        if (GameManager.instance.m_playerControllers.Count == 4)
        {
            
            for (int i = 0; i < GameManager.instance.m_playerControllers.Count; i++)
            {
                for (int n = i+1; n < GameManager.instance.m_playerControllers.Count; n++)
                {
                    if ((int)GameManager.instance.m_playerControllers[n].m_playerID <= (int)GameManager.instance.m_playerControllers[i].m_playerID)
                    {
                        PlayerController temp = GameManager.instance.m_playerControllers[n];
                        GameManager.instance.m_playerControllers[n] = GameManager.instance.m_playerControllers[i];
                        GameManager.instance.m_playerControllers[i] = temp;
                    }
                }
            }
        }

        

        switch (m_playerID) {
            case EPlayerID.Player1:
                m_lightLevel = GameManager.instance.m_currentLightningLevel1;
                m_waterLevel = GameManager.instance.m_currentWaterLevel1;
                m_fireLevel = GameManager.instance.m_currentFireLevel1;
                break;
            case EPlayerID.Player2:
                m_lightLevel = GameManager.instance.m_currentLightningLevel2;
                m_waterLevel = GameManager.instance.m_currentWaterLevel2;
                m_fireLevel = GameManager.instance.m_currentFireLevel2;
                break;
            case EPlayerID.Player3:
                m_lightLevel = GameManager.instance.m_currentLightningLevel3;
                m_waterLevel = GameManager.instance.m_currentWaterLevel3;
                m_fireLevel = GameManager.instance.m_currentFireLevel3;
                break;
            case EPlayerID.Player4:
                m_lightLevel = GameManager.instance.m_currentLightningLevel4;
                m_waterLevel = GameManager.instance.m_currentWaterLevel4;
                m_fireLevel = GameManager.instance.m_currentFireLevel4;
                break;
        }


        m_player = ReInput.players.GetPlayer((int)m_playerID);
        m_player.isPlaying = true;
        Resume();
        optionsCheck = GameObject.Find("OptionsGameObject");
        m_gameTimer = FindObjectOfType<Timer>();

        m_standardRange = m_standardRangeOrigin;
        m_standardSpeed = m_standardSpeedOrigin;
        m_standardRecoil = m_standardRecoilOrigin;
        m_bSpecialShoot = true;
        m_bHasCoin = false;
        m_bStartCountdownRunning = false;

        m_healthBarCanvas = Instantiate(m_healthBarCanvas);
        m_healthBarCanvas.transform.GetChild(2).gameObject.SetActive(false);
        m_specialPic = m_healthBarCanvas.transform.GetChild(1).transform.GetChild(0).GetComponent<Image>();

        m_specialReload = m_specialReloadMax;

        m_lineRenderer = GetComponent<LineRenderer>();

        m_bCanShoot = false;
        m_bIsInvulnerable = false;
        m_bDirection = true;

        m_invulCooldown = m_invulCooldownMax;

        m_weaponType = EWeaponType.DEFAULT;
        m_weaponLevel = EWeaponLevel.ZERO;

        for (int i = 0; i<transform.childCount; i++)
            m_spriteRendererArray = gameObject.GetComponentsInChildren<SpriteRenderer>();

        for (int i = 0; i < m_spriteRendererArray.Length; i++)
            m_spriteRendererList.Add(m_spriteRendererArray[i]);

        for (int i = 0; i < System.Enum.GetValues(typeof(EWeaponType)).Length-1; i++)
            m_particlesystems.Add(transform.GetChild(i).GetComponent<ParticleSystem>());
    }
	
	// Update is called once per frame
	void Update () {

        m_healthBarCanvas.transform.position = transform.position + Vector3.up * m_playerCanvasOffset;

        if (m_gameTimer.m_bNewRoundStarted)
        {
            GetInput();
            RotateArm();
        }

        if (GameManager.instance.m_bIsRunning)
        {
            GetInput();
            m_shootingDir = new Vector2(m_moveVector.x, m_moveVector.y);

            Raycast();
            RotateArm();

            if (m_player.GetButtonDown("menu"))
            {

                if (GameIsPaused && (optionsPausemenuUI.activeSelf == false))
                {
                    Resume();
                }

                else
                {
                    Pause();
                }

            }

            m_normalShotRecovery -= Time.deltaTime;

            m_normalShotRecovery = Mathf.Clamp(m_normalShotRecovery, -1, 1); //don't let the value go into infinity

            if (m_normalShotRecovery < 0)
                m_bCanShoot = true;
            else
                m_bCanShoot = false;

            if (m_bIsInvulnerable)
            {
                gameObject.layer = 11;

                m_invulCooldown -= Time.deltaTime;
                m_flashTimer -= Time.deltaTime;

                if (m_flashTimer < 0)
                {
                    for (int i = 0; i < m_spriteRendererList.Count; i++)
                        m_spriteRendererList[i].enabled = !m_spriteRendererList[i].enabled;

                    m_flashTimer = m_flashTimerMax;
                }
            }


            if (m_invulCooldown < 0)
            {
                m_bIsInvulnerable = false;
                gameObject.layer = 0;
                for (int i = 0; i < m_spriteRendererList.Count; i++)
                    m_spriteRendererList[i].enabled = true;
                m_invulCooldown = m_invulCooldownMax;
            }

            if (m_player.GetButton("Shoot"))
            {
                m_bPlayerHoldsButton = true;
                m_standardSpeed = Mathf.Lerp(m_standardSpeedOrigin, m_standardSpeedOrigin * 2, m_holdTimer / m_holdTimeMax);
                m_standardRecoil = Mathf.Lerp(m_standardRecoilOrigin, m_standardRecoilOrigin * 2, m_holdTimer / m_holdTimeMax);
                m_standardRange = Mathf.Lerp(m_standardRangeOrigin, m_standardRangeOrigin * 2, m_holdTimer / m_holdTimeMax);
            }
            else
                m_bPlayerHoldsButton = false;

            if (m_player.GetButtonUp("Shoot"))
            {
                if (m_bCanShoot && (m_moveVector.x != 0 || m_moveVector.y != 0))
                {
                    Shoot();
                }
            }

            if (m_bPlayerHoldsButton)
            {
                m_holdTimer += Time.deltaTime;
                m_holdTimer = Mathf.Clamp(m_holdTimer, 0f, m_holdTimeMax);
            }
            else
                m_holdTimer = 0f;

            if (m_bSpecialReload)
                m_specialReload += Time.deltaTime;

            m_specialPic.fillAmount = m_specialReload / m_specialReloadMax;


            if (m_player.GetButtonDown("ElementShoot") && m_bSpecialShoot)
            {
                switch (m_weaponType)
                {
                    case EWeaponType.FIRE:

                        switch (m_fireLevel)
                        {
                            case (1):
                                GameManager.instance.m_weaponElement.m_fireWeapon.FireShot(this, 1);
                                break;
                            case (2):
                                GameManager.instance.m_weaponElement.m_fireWeapon.FireShot(this, 2);
                                break;
                            case (3):
                                GameManager.instance.m_weaponElement.m_fireWeapon.FireShot(this, 3);
                                break;
                            default:
                                GameManager.instance.m_weaponElement.m_fireWeapon.FireShot(this, 4);
                                break;
                        }

                        break;

                    case EWeaponType.LIGHTNING:

                        switch (m_lightLevel)
                        {
                            case (1):
                                GameManager.instance.m_weaponElement.m_lightningWeapon.LightningShot1(this);
                                break;
                            case (2):
                                GameManager.instance.m_weaponElement.m_lightningWeapon.LightningShot2(this);
                                break;
                            case (3):
                                GameManager.instance.m_weaponElement.m_lightningWeapon.LightningShot3(this);
                                break;
                            default:
                                GameManager.instance.m_weaponElement.m_lightningWeapon.LightningShot4(this);
                                break;
                        }



                        break;

                    

                    case EWeaponType.WATER:

                        switch (m_waterLevel)
                        {
                            case (1):
                                GameManager.instance.m_weaponElement.m_waterWeapon.WaterShot1(this);
                                break;
                            case (2):
                                GameManager.instance.m_weaponElement.m_waterWeapon.WaterShot2(this);
                                break;
                            case (3):
                                GameManager.instance.m_weaponElement.m_waterWeapon.WaterShot3(this);
                                break;
                            default:
                                GameManager.instance.m_weaponElement.m_waterWeapon.WaterShot4(this);
                                break;

                        }


                        break;
                }

            }

            if (m_specialReload >= m_specialReloadMax)
            {
                m_bSpecialReload = false;
                m_bSpecialShoot = true;
            }
        }

    }

    //normal Shot
    void Shoot()
    {
        GameManager.instance.m_weaponElement.m_earthWeapon.EarthShot(this);
        m_standardRange = m_standardRangeOrigin;
        m_standardSpeed = m_standardSpeedOrigin;
        m_standardRecoil = m_standardRecoilOrigin;

        m_normalShotRecovery = m_normalShotRecoveryMax;
    }

    public void GetInput()
    {
        m_moveVector.x = m_player.GetAxis("MoveHorizontal");
        m_moveVector.y = m_player.GetAxis("MoveVertical");
    }

    private void RotateArm()
    {
        // Mathf.Atan2(-YAxis, XAxis) * Mathf.Rad2Deg defines the rotation
        float XAxis = m_moveVector.x;
        float YAxis = -m_moveVector.y;

        float rotZ = Mathf.Atan2(-YAxis, XAxis) * Mathf.Rad2Deg;
        m_arm.rotation = Quaternion.Euler(0f, 0f, rotZ /*rotation offset*/);

        float angle = Mathf.Atan2(m_moveVector.y, m_moveVector.x) * Mathf.Rad2Deg;
        SpriteRenderer m_spriteRenderer;
        m_spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();


        if (angle > 0f && angle < 100f || angle < 0f && angle > -90f)
        {
            if (m_bDirection == false)

            {
                m_bDirection = true;



                m_spriteRenderer.flipX = false;
            }
        }

        if (angle > 100f && angle < 180f || angle < -90f && angle > -180f)

            if (m_bDirection == true)

            {
                m_bDirection = false;

                m_spriteRenderer.flipX = true;
            }

    }

    private void Raycast()
    {
        Ray2D ray = new Ray2D(m_firePoint.position, new Vector2(m_moveVector.x, m_moveVector.y));
        m_lineRenderer.SetPosition(0, m_firePoint.position);
        m_lineRenderer.SetPosition(1, new Vector2 (m_firePoint.position.x, m_firePoint.position.y) + new Vector2 (m_moveVector.x, m_moveVector.y).normalized * m_standardRange);
    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);

        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    public void Pause()
    {

        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
   
}
