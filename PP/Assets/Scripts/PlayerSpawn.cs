﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {

	// Use this for initialization
	void Start () {

        GameManager.instance.m_playerControllers = new List<PlayerController>();
    }

    public void TwoPlayers()
    {
        GameManager.instance.SetPlayerNumber(2);
    }

   public void ThreePlayers()
    {
        GameManager.instance.SetPlayerNumber(3);
    }

    public void FourPlayers()
    {
        GameManager.instance.SetPlayerNumber(4);
    }
    
}
