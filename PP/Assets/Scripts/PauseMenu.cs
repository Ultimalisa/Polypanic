﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PauseMenu : MonoBehaviour {

    public Button m_resumeButton;
    public UnityEngine.EventSystems.EventSystem m_eventSystem;

    private void Awake()
    {
        m_eventSystem.SetSelectedGameObject(m_resumeButton.gameObject);
    }

    private void Update()
    {
        if (m_eventSystem.currentSelectedGameObject == null)
        {
            m_eventSystem.SetSelectedGameObject(m_resumeButton.gameObject);
        }        
    }

    // Use this for initialization
    public void  BacktoMenu()
    {
        GameManager.instance.BackToMenu();
    }

    public void Resume()
    {

    }
}
