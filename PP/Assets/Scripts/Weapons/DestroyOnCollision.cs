﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour {

    public EPlayerID m_bulletID;

    private void OnCollisionEnter2D(Collision2D _collision)
    {
        if ((_collision.gameObject.tag == "player") && (m_bulletID == _collision.gameObject.GetComponent<PlayerController>().m_playerID))
        {
            return;
        }
        else if ((_collision.gameObject.tag == "player") && (m_bulletID != _collision.gameObject.GetComponent<PlayerController>().m_playerID) && !_collision.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable)
        {
            _collision.gameObject.GetComponent<Health>().Damage(1);
            _collision.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable = true;

            Destroy(this.gameObject);
        }
    }
}
