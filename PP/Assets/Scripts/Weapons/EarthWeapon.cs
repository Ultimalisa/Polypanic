﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthBullet
{
    public GameObject m_bulletObject;
    public Vector2 m_startPosition;
    
}

public class EarthWeapon : WeaponLevel {

    public AudioSource m_earthSound;
    [SerializeField]
    internal GameObject m_earthProjectile;
    internal List<EarthBullet> m_earthBulletList;
    internal float m_currentRange;

    private void Start()
    {
        m_earthBulletList = new List<EarthBullet>();
    }

    private void Update()
    {
        for (int i = 0; i < m_earthBulletList.Count; i++)
        {
            if (m_earthBulletList.Count != 0)
            {
                
                if (m_earthBulletList[i].m_bulletObject != null)
                {
                    if ((Mathf.Abs(Vector2.Distance(m_earthBulletList[i].m_bulletObject.transform.position, m_earthBulletList[i].m_startPosition))) > m_currentRange)
                    {
                        Destroy(m_earthBulletList[i].m_bulletObject);
                    }
                }
                if (m_earthBulletList[i].m_bulletObject == null)
                    m_earthBulletList.Remove(m_earthBulletList[i]);
            }
        }
    }

    public void EarthShot(PlayerController _pc)
    {
        m_currentRange = _pc.m_standardRange;

        float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
        float YAxis = _pc.m_player.GetAxis("MoveVertical");

        Vector2 shootingDir = new Vector2(XAxis, YAxis);

        GameObject bullet = GameObject.Instantiate<GameObject>(m_earthProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);

        m_earthSound.Play();
        EarthBullet earthBullet = new EarthBullet();
        bullet.GetComponent<HitDetection>().bulletID = _pc.m_playerID;
        earthBullet.m_bulletObject = bullet;
        earthBullet.m_startPosition = bullet.transform.position;
        m_earthBulletList.Add(earthBullet);
        earthBullet.m_bulletObject.GetComponent<Rigidbody2D>().velocity = shootingDir.normalized * _pc.m_standardSpeed;

        if (Vector2.Dot(-shootingDir, _pc.GetComponent<Rigidbody2D>().velocity) > 0)
            _pc.GetComponent<Rigidbody2D>().velocity += -shootingDir.normalized * _pc.m_standardRecoil;
        else
            _pc.GetComponent<Rigidbody2D>().velocity = -shootingDir.normalized * _pc.m_standardRecoil;

        //Debug.Log(Vector2.Dot(-shootingDir, _pc.GetComponent<Rigidbody2D>().velocity));
    }
}


