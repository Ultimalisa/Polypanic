﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponElement : MonoBehaviour {

    public FireWeapon m_fireWeapon;
    public LightningWeapon m_lightningWeapon;
    public WaterWeapon m_waterWeapon;
    public EarthWeapon m_earthWeapon;

	// Use this for initialization
	void Start () {
        GameManager.instance.m_weaponElement = this;
	}
}
