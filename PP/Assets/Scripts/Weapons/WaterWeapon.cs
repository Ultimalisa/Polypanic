﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Spine.Unity;


public class WaterWeapon : WeaponLevel {
    // public float coolDown = 3;
    public float coolDownTimer = 3;
    public GameObject water;
    public GameObject rainbow;
  //  public float delay = 5f;
    public LayerMask notToHit;
    bool canShoot = false;

    public Transform FirePoint;
    public float FireForce1 = 1000f;
    public GameObject waterBeam;
    public AudioSource m_laserSound;
    public AudioSource m_rainbowSound;

    internal int m_scaleAdaption = 50;

    //public Rigidbody2D rb;
    // Use this for initialization
    void Start() {
        FireForce1 = 1000f;
    }

    // Update is called once per frame
    void Update() {

        

        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

       // Debug.Log(coolDownTimer);


    }
    public void WaterShot1( PlayerController _pc)
    {
        // m_currentLevel = m_level[0];
        _pc.m_specialReloadMax = 3f;
        if (_pc.m_bSpecialShoot)
        {
            m_laserSound.Play();
            FireForce1 = FireForce1 * 0.55f;
            StartCoroutine(WaitWater( _pc,0.5f,0.75f,0.25f));

            _pc.m_specialReload = 0;
            _pc.m_bSpecialReload = true;
            _pc.m_bSpecialShoot = false;

        }

       
    }
    public void WaterShot2(PlayerController _pc)
    {
        _pc.m_specialReloadMax = 3f;
        if (_pc.m_bSpecialShoot)
        {
            m_laserSound.Play();
            FireForce1 = FireForce1 * 0.66f;
            StartCoroutine(WaitWater( _pc, 0.5f, 1,0.4f));

            _pc.m_specialReload = 0;
            _pc.m_bSpecialReload = true;
            _pc.m_bSpecialShoot = false;

        }

       
    }
    public void WaterShot3(PlayerController _pc)
    {
        _pc.m_specialReloadMax = 3f; ;
        if (_pc.m_bSpecialShoot)
        {
            m_laserSound.Play();
            FireForce1 = FireForce1 * 0.80f;
            StartCoroutine(WaitWater( _pc, 0.75f, 1,0.6f));
            _pc.m_specialReload = 0;
            _pc.m_bSpecialReload = true;
            _pc.m_bSpecialShoot = false;


        }
    }
    public void WaterShot4(PlayerController _pc)
    {
        _pc.m_specialReloadMax = 3f;
        if (_pc.m_bSpecialShoot)
        {
            m_rainbowSound.Play();
           // _pc.canShoot = false;
            StartCoroutine(WaitRainbow(  _pc));
            _pc.m_specialReload = 0;
            _pc.m_bSpecialReload = true;
            _pc.m_bSpecialShoot = false;


        }

        
    }



    void AnimationTimer()
    {


    }

    IEnumerator WaitRainbow( PlayerController _pc)
    {
        
        float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
        float YAxis = -_pc.m_player.GetAxis("MoveVertical");

        coolDownTimer = 3.3f;
        Vector2 FirePointPosition = new Vector2(_pc.m_firePoint.position.x, _pc.m_firePoint.position.y);

        GameObject go2 = (GameObject)Instantiate(rainbow, FirePointPosition, _pc.m_firePoint.rotation, null);
        Spine.Skeleton skeleton = go2.GetComponent<SkeletonAnimation>().skeleton;
        skeleton.a = 0.2f;
        go2.GetComponent<Waterdetection>().bulletID = _pc.m_playerID;
      //  go1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.3f);
        //  this.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
      

       

        //go1.GetComponent<>().bulletID = m_playerID;

        yield return new WaitForSeconds(0.3f);

        skeleton.a = 1f;


   

       // go2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

        BackforceC(FireForce1,  _pc);
        //  this.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);


        // go1.GetComponent<Rigidbody2D>().velocity = new Vector2(XAxis * -1, (-YAxis) * -1).normalized * (FireForce1 * Time.deltaTime);
        //  go1.GetComponent<Waterdetection>().bulletID = _pc.m_playerID;

        //go1.GetComponent<>().bulletID = m_playerID;

        _pc.m_bCanShoot = false;
         FireForce1 = 1000f;
}

    IEnumerator WaitWater( PlayerController _pc,float _x,float _y,float _z)
    {
        float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
        float YAxis = -_pc.m_player.GetAxis("MoveVertical");

        Vector2 shootingDir = new Vector2(XAxis, -YAxis);

        Vector2 offset = shootingDir.normalized * 10; //offset der Länge 10

        coolDownTimer = 3.3f;
        float middle = gameObject.transform.localScale.x / 2;
        Vector2 FirePointPosition = new Vector2(_pc.m_firePoint.position.x,_pc.m_firePoint.position.y);
        Vector2 EndPointPosition = new Vector2(XAxis, -YAxis);
        
        RaycastHit2D hit = Physics2D.Linecast(FirePointPosition + shootingDir.normalized, FirePointPosition + offset * 100, 13 << LayerMask.NameToLayer("Wall"));
       
       Debug.DrawLine(FirePointPosition +shootingDir.normalized, FirePointPosition + offset * 100, Color.magenta, 3f);
        
        GameObject go1 = (GameObject)Instantiate(water, FirePointPosition, _pc.m_firePoint.rotation, null);

        Spine.Skeleton skeleton = go1.GetComponent<SkeletonAnimation>().skeleton;
        skeleton.a = 0.2f;
        go1.GetComponent<Waterdetection>().bulletID = _pc.m_playerID;
        //  go1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.3f);
        go1.GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f, 0.2f);
        go1.transform.localScale = new Vector2(go1.transform.localScale.x * _x * _y, go1.transform.localScale.y*_z);
        
        if(hit.collider != null)
        {
            float xpoint = hit.collider.transform.position.x;
            float ypoint = hit.collider.transform.position.y;
            Vector2 wall = new Vector2(xpoint, ypoint);
            float distance = Mathf.Abs(Vector2.Distance(FirePointPosition, wall));
            Debug.Log("distance"+distance);
           if (distance/m_scaleAdaption < go1.transform.localScale.x)
            {
                go1.transform.localScale = new Vector2(distance/ m_scaleAdaption, go1.transform.localScale.y);
            }
        }

        //go1.GetComponent<>().bulletID = m_playerID;

        yield return new WaitForSeconds(0.3f);

        skeleton.a = 1f;


        _pc.m_bCanShoot = false;
       // go1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

        BackforceC( FireForce1,_pc);
        go1.GetComponent<Renderer>().material.SetColor("_Color", new Color(1, 1, 1, 1f));


        // go1.GetComponent<Rigidbody2D>().velocity = new Vector2(XAxis * -1, (-YAxis) * -1).normalized * (FireForce1 * Time.deltaTime);
        //  go1.GetComponent<Waterdetection>().bulletID = m_playerID;

        //go1.GetComponent<>().bulletID = m_playerID;
        FireForce1 = 1000f;
       
    }


    void BackforceC(float fireforce,  PlayerController _pc)
    {

        float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
        float YAxis = -_pc.m_player.GetAxis("MoveVertical");

        //Rigidbody2D rb = _pc.GetComponent<Rigidbody2D>();

        Vector2 vec = new Vector2(XAxis, -YAxis).normalized * fireforce;

        Debug.Log(vec);

        _pc.GetComponent<Rigidbody2D>().velocity = new Vector2(-XAxis , YAxis).normalized * fireforce;
        _pc.GetComponent<Rigidbody2D>().drag = 1;
    }
}
