﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplosion : MonoBehaviour {

    private AudioSource m_explosionSound;

    private void Awake()
    {
        m_explosionSound = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if (_collision.gameObject.tag == "player" && !_collision.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable)
        {
            m_explosionSound.Play();

            _collision.gameObject.GetComponent<Health>().Damage(1);
            _collision.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable = true;

        }

        if (_collision.gameObject.tag == "stone"&& _collision.gameObject.tag != "blackhole")
        {
            m_explosionSound.Play();

            _collision.gameObject.GetComponent<RandomItemDrop>().SpawnItem();
            Destroy(_collision.gameObject); 
        }
    }
}
