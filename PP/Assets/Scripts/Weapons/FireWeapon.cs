﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class FireBullet
{
    public GameObject m_bulletObject;
    public Vector2 m_startPosition;
}

public class FireWeapon : WeaponLevel {

    public List<GameObject> m_bombRange;
    internal GameObject m_currentBombPrefab;
    public GameObject m_fireProjectile;
    private GameObject m_fireBullet;
    [SerializeField]
    internal List<FireBullet> m_fireBulletList;
    [SerializeField]
    internal GameObject m_pacman;
    public AudioSource m_fireStartSound;
    public AudioSource m_exploisionSound;
    public AudioSource m_exploisionSoundLevel4;
    public AudioSource m_pacmanSound;

    private void Awake()
    {
        m_fireBulletList = new List<FireBullet>();
    }

    private void Update()
    {

        if (m_fireBulletList.Count != 0)
        {
            for (int i = 0; i<m_fireBulletList.Count; i++)
            {
                if ((Mathf.Abs(Vector2.Distance(m_fireBulletList[i].m_bulletObject.transform.position, m_fireBulletList[i].m_startPosition))) > m_currentLevel.m_range)
                {
                    if (m_currentLevel == m_level[3])
                        m_exploisionSoundLevel4.Play();
                    else
                        m_exploisionSound.Play();

                    GameObject bomb = (GameObject)Instantiate(m_currentBombPrefab, m_fireBulletList[i].m_bulletObject.transform.position, m_fireBulletList[i].m_bulletObject.transform.rotation, null);
                    Destroy(m_fireBulletList[i].m_bulletObject);
                    m_fireBulletList.Remove(m_fireBulletList[i]);
                }
            }
        }
    }

    public void FireShot(PlayerController _pc, int level)
    {
        m_currentLevel = m_level[level-1];
        _pc.m_specialReloadMax = m_currentLevel.m_reloadMax;

        m_currentBombPrefab = m_bombRange[level-1];
        if (_pc.m_bSpecialShoot)
        {
            if (_pc.m_moveVector.x != 0 || _pc.m_moveVector.y != 0)
            {
                
                if(level < 4)
                    m_fireStartSound.PlayScheduled(0.5);
                else
                    m_pacmanSound.Play();

                Vector2 FirePointPosition = new Vector2(_pc.m_firePoint.position.x, _pc.m_firePoint.position.y);

                GameObject go = level<4 ? m_fireProjectile : m_pacman;

                GameObject newFireBullet = (GameObject)Instantiate(go, FirePointPosition, _pc.m_firePoint.rotation, null);
                newFireBullet.GetComponent<ExplodeOnCollision>().m_bulletID = _pc.m_playerID;

                float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
                float YAxis = -_pc.m_player.GetAxis("MoveVertical");

                Vector2 shootingDir = new Vector2(XAxis, -YAxis).normalized;

                FireBullet fireBullet = new FireBullet();
                fireBullet.m_bulletObject = newFireBullet;
                fireBullet.m_startPosition = fireBullet.m_bulletObject.transform.position;
                newFireBullet.GetComponent<Rigidbody2D>().velocity = shootingDir.normalized * m_currentLevel.m_speed;
                _pc.GetComponent<Rigidbody2D>().velocity = -shootingDir * m_currentLevel.m_recoil;
                m_fireBulletList.Add(fireBullet);

                

                _pc.m_specialReload = 0;
                _pc.m_bSpecialReload = true;
                _pc.m_bSpecialShoot = false;
            }
        }
    }
}
