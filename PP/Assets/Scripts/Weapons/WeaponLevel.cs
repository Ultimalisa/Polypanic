﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

[System.Serializable]
public class Level
{
    public float m_range;
    public float m_speed;
    public float m_recoil;
    public float m_reloadMax;

    public Level(float _range, float _speed, float _recoil, float _cd)
    {
        m_range = _range;
        m_speed = _speed;
        m_reloadMax = _cd;
    }
}

public class WeaponLevel : MonoBehaviour
{
    [SerializeField]
    protected Level[] m_level;
    protected Level m_currentLevel;
    protected bool m_bIsAllowedToShoot;
    protected float m_fireReload;
}