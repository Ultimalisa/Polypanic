﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

     void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag != "player")
        {
            Destroy(other.gameObject);
        }
        else
        {
            other.gameObject.GetComponent<Health>().Damage(1);
            other.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable = true;
        }
    }
}
