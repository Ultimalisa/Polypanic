﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//// Vector (x,y) um phi rotiert:
//// xRot = x *cos(phi)  -  y *sin(phi)
//// yRot = x *sin(phi)  +  y *cos(phi)

public class LightningBullet
{
    public GameObject m_bulletObject;
    public Vector2 m_startPosition;
}

public class RotateBullets
{
    public Transform m_playerTransform;
    public GameObject m_rotateBullet;
    public bool m_bRotate = false;
}

public class LightningWeapon : WeaponLevel {

    [SerializeField]
    internal GameObject m_lightningProjectile;

    internal List<LightningBullet> m_lightningBulletList;
    [SerializeField]
    internal float m_count;  //the amount of bullets that are spawning in Level 4
    [SerializeField]
    internal float m_offsetL4; //the distance between the initial point of the bullets in Level 4 and the player position
    internal bool m_bMove;
    [SerializeField]
    internal bool m_bRotateL4;
    internal List<RotateBullets> m_rotateObjectList;

    public AudioSource m_lightSound;

    // Use this for initialization
    void Start () {
        m_lightningBulletList = new List<LightningBullet>();
        m_rotateObjectList = new List<RotateBullets>();
        m_bMove = false;
    }
	
	// Update is called once per frame
	void Update () {

     
        for (int i = 0; i < m_rotateObjectList.Count; i++)
        {
            if (m_rotateObjectList[i].m_bRotate)
            {
                if (m_rotateObjectList[i].m_rotateBullet != null)
                {

                    m_rotateObjectList[i].m_rotateBullet.transform.RotateAround(m_rotateObjectList[i].m_playerTransform.position, Vector3.forward, 5);

                    Vector3 direction = m_rotateObjectList[i].m_rotateBullet.transform.position - m_rotateObjectList[i].m_playerTransform.position;
                    m_rotateObjectList[i].m_rotateBullet.GetComponent<Rigidbody2D>().velocity = direction.normalized * m_currentLevel.m_speed;
                }
                else
                    m_rotateObjectList.Remove(m_rotateObjectList[i]);
            }
        }
        

        for (int i = 0; i < m_lightningBulletList.Count; i++)
        {
            if (m_lightningBulletList.Count != 0)
            {
                
                if (m_lightningBulletList[i].m_bulletObject != null)
                {
                    if ((Mathf.Abs(Vector2.Distance(m_lightningBulletList[i].m_bulletObject.transform.position, m_lightningBulletList[i].m_startPosition))) > m_currentLevel.m_range)
                    {
                        Destroy(m_lightningBulletList[i].m_bulletObject);
                    }
                }
                if (m_lightningBulletList[i].m_bulletObject == null)
                    m_lightningBulletList.Remove(m_lightningBulletList[i]);
            }
        }
    }

    public void LightningShot1(PlayerController _pc)
    {
        m_currentLevel = m_level[0];
        _pc.m_specialReloadMax = m_currentLevel.m_reloadMax;

        if (_pc.m_bSpecialShoot)
        {
            if (_pc.m_moveVector.x != 0 || _pc.m_moveVector.y != 0)
            {

                float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
                float YAxis = -_pc.m_player.GetAxis("MoveVertical");

                Vector2 shootingDir = new Vector2(XAxis, -YAxis);

                GameObject lightning1 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);

                m_lightSound.Play();

                lightning1.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;
                LightningBullet lightningBullet1 = new LightningBullet();
                lightningBullet1.m_bulletObject = lightning1;
                lightningBullet1.m_startPosition = lightningBullet1.m_bulletObject.transform.position;
                lightning1.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning1, 0.5f).normalized * m_currentLevel.m_speed;
                m_lightningBulletList.Add(lightningBullet1);

                GameObject lightning2 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning2.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;
                LightningBullet lightningBullet2 = new LightningBullet();
                lightningBullet2.m_bulletObject = lightning2;
                lightningBullet2.m_startPosition = lightningBullet2.m_bulletObject.transform.position;
                lightning2.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning2, -0.5f).normalized * m_currentLevel.m_speed;
                m_lightningBulletList.Add(lightningBullet2);

                _pc.GetComponent<Rigidbody2D>().velocity = -shootingDir.normalized * m_currentLevel.m_recoil;

                _pc.m_specialReload = 0;
                _pc.m_bSpecialReload = true;
                _pc.m_bSpecialShoot = false;
            }
        }
    }
    public void LightningShot2(PlayerController _pc)
    {
        m_currentLevel = m_level[1];
        _pc.m_specialReloadMax = m_currentLevel.m_reloadMax;

        if (_pc.m_bSpecialShoot)
        {
            if (_pc.m_moveVector.x != 0 || _pc.m_moveVector.y != 0)
            {
                float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
                float YAxis = -_pc.m_player.GetAxis("MoveVertical");

                Vector2 shootingDir = new Vector2(XAxis, -YAxis);

                m_lightSound.Play();


                GameObject lightning1 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning1.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;
                LightningBullet lightningBullet1 = new LightningBullet();
                lightningBullet1.m_bulletObject = lightning1;
                lightningBullet1.m_startPosition = lightningBullet1.m_bulletObject.transform.position;
                lightning1.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning1, 45).normalized * m_currentLevel.m_speed;
                m_lightningBulletList.Add(lightningBullet1);

                GameObject lightning2 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning2.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;
                LightningBullet lightningBullet2 = new LightningBullet();
                lightningBullet2.m_bulletObject = lightning2;
                lightningBullet2.m_startPosition = lightningBullet2.m_bulletObject.transform.position;
                lightning2.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning2, 0).normalized * m_currentLevel.m_speed;
                m_lightningBulletList.Add(lightningBullet2);

                GameObject lightning3 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning3.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;
                LightningBullet lightningBullet3 = new LightningBullet();
                lightningBullet3.m_bulletObject = lightning3;
                lightningBullet3.m_startPosition = lightningBullet3.m_bulletObject.transform.position;
                lightning3.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning3, -45).normalized * m_currentLevel.m_speed;
                m_lightningBulletList.Add(lightningBullet3);

                _pc.GetComponent<Rigidbody2D>().velocity = -shootingDir.normalized * m_currentLevel.m_recoil;

                _pc.m_specialReload = 0;
                _pc.m_bSpecialReload = true;
                _pc.m_bSpecialShoot = false;
            }
        }
    }
    public void LightningShot3(PlayerController _pc)
    {
        m_currentLevel = m_level[2];
        _pc.m_specialReloadMax = m_currentLevel.m_reloadMax;

        if (_pc.m_bSpecialShoot)
        {
            if (_pc.m_moveVector.x != 0 || _pc.m_moveVector.y != 0)
            {

                m_lightSound.Play();

                //Create Gameobjects and set IDs
                GameObject lightning1 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning1.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;

                GameObject lightning2 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning2.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;

                GameObject lightning3 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning3.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;

                GameObject lightning4 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning4.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;

                GameObject lightning5 = GameObject.Instantiate(m_lightningProjectile, _pc.m_firePoint.position, _pc.m_firePoint.rotation, null);
                lightning5.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;

                //Give them an ID
                //for (int i = 0; i < m_lightningBulletList.Count; i++)
                //    m_lightningBulletList[i].m_bulletObject.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;

                float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
                float YAxis = -_pc.m_player.GetAxis("MoveVertical");
                float RotZ = Mathf.Atan2(-YAxis, XAxis) * Mathf.Rad2Deg;
                Vector2 RotationDir = new Vector2(XAxis, -YAxis);

                LightningBullet lightningBullet1 = new LightningBullet();
                lightningBullet1.m_bulletObject = lightning1;
                lightningBullet1.m_startPosition = lightningBullet1.m_bulletObject.transform.position;
                lightning1.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning1, 0.5f).normalized * 500;
                lightning1.transform.rotation = Quaternion.Euler(RotationDir.x, RotationDir.y, RotZ + 30);
                m_lightningBulletList.Add(lightningBullet1);

                LightningBullet lightningBullet2 = new LightningBullet();
                lightningBullet2.m_bulletObject = lightning2;
                lightningBullet2.m_startPosition = lightningBullet2.m_bulletObject.transform.position;
                lightning2.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning2, 45).normalized * 500;
                m_lightningBulletList.Add(lightningBullet2);

                LightningBullet lightningBullet3 = new LightningBullet();
                lightningBullet3.m_bulletObject = lightning3;
                lightningBullet3.m_startPosition = lightningBullet3.m_bulletObject.transform.position;
                lightning3.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning3, 0).normalized * 500;
                m_lightningBulletList.Add(lightningBullet3);


                LightningBullet lightningBullet4 = new LightningBullet();
                lightningBullet4.m_bulletObject = lightning4;
                lightningBullet4.m_startPosition = lightningBullet4.m_bulletObject.transform.position;
                lightning4.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning4, -0.5f).normalized * 500;
                lightning4.transform.rotation = Quaternion.Euler(RotationDir.x, RotationDir.y, RotZ - 30);
                m_lightningBulletList.Add(lightningBullet4);

                LightningBullet lightningBullet5 = new LightningBullet();
                lightningBullet5.m_bulletObject = lightning5;
                lightningBullet5.m_startPosition = lightningBullet5.m_bulletObject.transform.position;
                lightning5.GetComponent<Rigidbody2D>().velocity = Rotate(_pc, lightning5, -45).normalized * 500;
                m_lightningBulletList.Add(lightningBullet5);

                

                _pc.GetComponent<Rigidbody2D>().velocity = -new Vector2(XAxis, -YAxis).normalized * m_currentLevel.m_recoil;

                _pc.m_specialReload = 0;
                _pc.m_bSpecialReload = true;
                _pc.m_bSpecialShoot = false;
            }
        }
    }

    public void LightningShot4(PlayerController _pc)
    {
        m_currentLevel = m_level[3];
        _pc.m_specialReloadMax = m_currentLevel.m_reloadMax;

        if (_pc.m_bSpecialShoot)
        {
            //if (_pc.m_moveVector.x != 0 || _pc.m_moveVector.y != 0)
            {

                m_count = 12;

                //List<GameObject> LB = new List<GameObject>();

                for (int i = 0; i<m_count; i++)
                {
                    GameObject lightning = GameObject.Instantiate(m_lightningProjectile, new Vector2(-10000, -10000), Quaternion.Euler(new Vector3(0f, 0f, 0f)), null);
                    lightning.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;
                    lightning.transform.position = _pc.transform.position + Vector3.up * m_offsetL4;
                    lightning.transform.RotateAround(lightning.transform.position, Vector3.forward, 90);
                    lightning.transform.RotateAround(_pc.transform.position, Vector3.forward, 30 * i);
                    LightningBullet myBullet = new LightningBullet();
                    myBullet.m_bulletObject = lightning;
                    myBullet.m_startPosition = lightning.transform.position;
                    RotateBullets rotateBullet = new RotateBullets();
                    rotateBullet.m_rotateBullet = lightning;
                    rotateBullet.m_playerTransform = _pc.transform;
                    rotateBullet.m_bRotate = true;
                    m_lightningBulletList.Add(myBullet);
                    m_rotateObjectList.Add(rotateBullet);
                }

                //Give them an ID
                //for (int i = 0; i < m_lightningBulletList.Count; i++)
                //    m_lightningBulletList[i].m_bulletObject.GetComponent<DestroyOnCollision>().m_bulletID = _pc.m_playerID;

                m_bMove = true;

                _pc.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

                _pc.m_specialReload = 0;
                _pc.m_bSpecialReload = true;
                _pc.m_bSpecialShoot = false;
            }
        }
    }

    private Vector2 Rotate(PlayerController _pc, GameObject _bullet, float _angle)
    {
        float XAxis = _pc.m_player.GetAxis("MoveHorizontal");
        float YAxis = -_pc.m_player.GetAxis("MoveVertical");

        float RotZ = Mathf.Atan2(-YAxis, XAxis) * Mathf.Rad2Deg;

        Vector2 ShootingDir;
        Vector2 RotationDir = new Vector2(XAxis, -YAxis);

        float XrotUp = XAxis * Mathf.Cos(_angle) - (-YAxis) * Mathf.Sin(_angle);
        float YrotUp = XAxis * Mathf.Sin(_angle) + (-YAxis) * Mathf.Cos(_angle);

        ShootingDir = new Vector2(XrotUp, YrotUp);
        _bullet.transform.rotation = Quaternion.Euler(RotationDir.x, RotationDir.y, RotZ + _angle);

        //Debug.Log(ShootingDir);
        return ShootingDir;
        
    }

}
