﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeOnCollision : MonoBehaviour {

    public EPlayerID m_bulletID;

    private FireWeapon m_fireweapon;
    internal GameObject m_bombExplosion;

	// Use this for initialization
	void Start () {
        m_fireweapon = FindObjectOfType<FireWeapon>();
	}

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        //Debug.Log(m_bulletID);
        if ((_collision.gameObject.tag == "player") && (m_bulletID == _collision.gameObject.GetComponent<PlayerController>().m_playerID))
        {
            return;
        }
        else
        {
            if (_collision.gameObject.tag != "blackhole")
            {
                m_bombExplosion = m_fireweapon.m_currentBombPrefab.gameObject;
                GameObject bomb = (GameObject)Instantiate(m_bombExplosion, transform.position, transform.rotation, null);

                for (int i = 0; i < m_fireweapon.m_fireBulletList.Count; i++)
                {
                    if (this.gameObject == m_fireweapon.m_fireBulletList[i].m_bulletObject)
                        m_fireweapon.m_fireBulletList.Remove(m_fireweapon.m_fireBulletList[i]);
                }

                Destroy(this.gameObject);
            }
        }
    }
}
