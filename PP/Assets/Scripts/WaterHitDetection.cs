﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterHitdetection : MonoBehaviour
{


    public EPlayerID bulletID;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        //Debug.Log(collision.gameObject.tag);

        if ((collision.gameObject.tag == "player") && (bulletID != collision.gameObject.GetComponent<PlayerController>().m_playerID))
        {

            collision.gameObject.GetComponent<Health>().Damage(1);
            Destroy(this.gameObject);
        }
      
    }
}
