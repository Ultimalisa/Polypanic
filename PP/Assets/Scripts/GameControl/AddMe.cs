﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddMe : MonoBehaviour {

    public void Add()
    {
        if (GameManager.instance.m_playerUIAbove == null)
            GameManager.instance.m_playerUIAbove = this.gameObject;

        GameManager.instance.m_playerUIAbove.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_winPlayerOne.ToString();
        GameManager.instance.m_playerUIAbove.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_winPlayerTwo.ToString();
        GameManager.instance.m_playerUIAbove.transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_winPlayerThree.ToString();
        GameManager.instance.m_playerUIAbove.transform.GetChild(2).transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_winPlayerFour.ToString();

    }
}
