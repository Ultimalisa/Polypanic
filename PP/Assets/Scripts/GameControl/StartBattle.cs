﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

public class StartBattle : MonoBehaviour {

    public float startDelay = 0.1f;
    private Player m_player;

    // Use this for initialization
    void Awake () {
		m_player = ReInput.players.GetPlayer(0);
    }
	
	// Update is called once per frame
	void Update () {
        if (m_player.GetButtonDown("Shoot"))
        {
            Debug.Log("A");
            Invoke("startBattle", startDelay);
        }
        if (m_player.GetButtonDown("ElementShoot"))
        {
            Debug.Log("B");
            Invoke("EndGame", startDelay);
        }
    }

    void startBattle()
    {
        Debug.Log("Next Scene");
        SceneManager.LoadScene(1);
    }
    void EndGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
