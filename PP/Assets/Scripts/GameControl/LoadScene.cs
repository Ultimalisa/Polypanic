﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadScene : MonoBehaviour {
    int sceneIndex;
	// Use this for initialization
	void Start () {
        sceneIndex = Random.Range(1,  (int)SceneManager.sceneCountInBuildSettings);
	}
	
	// Update is called once per frame
	void Update () {
      
	}

    public void LoadNextScene()
    {
          StartCoroutine(LoadSceneAsynchonously());
    }

    IEnumerator LoadSceneAsynchonously ()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while(!operation.isDone)
        {
            Debug.Log(operation.progress);
            yield return null;
        }
    }
}
