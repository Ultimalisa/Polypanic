﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float m_timeLeft = 300f;
    private float m_timeOriginal;
    [SerializeField]
    internal float m_countdown = 6f;
    private float m_countdownMax;
    internal bool m_bNewRoundStarted;
    private bool m_bStartTimer;

    [SerializeField]
    internal Text m_timeText;
    [SerializeField]
    Text m_countdownText;

    private void Start()
    {
        m_bNewRoundStarted = true;
        m_bStartTimer = false;
        m_timeOriginal = m_timeLeft;
        m_countdownMax = m_countdown;
    }


    private void Update()
    {
        //if (GameManager.instance.m_bIsRunning)
        {
            if (m_bNewRoundStarted)
                StartCountdown();
            if (m_countdown < 0)
            {
                m_countdownText.gameObject.SetActive(false);
                m_bNewRoundStarted = false;
                m_countdown = m_countdownMax;

                m_timeText.gameObject.SetActive(true);

                m_bStartTimer = true;

                if (m_timeLeft < 0)
                {
                    GameManager.instance.m_bIsRunning = false;
                    StopTime();
                }
            }
            if (m_bStartTimer)
                StartTimer();
        }
    }

    public void StartTimer()
    {

        float minutes = Mathf.Floor(m_timeLeft / 60);
        float seconds = Mathf.RoundToInt(m_timeLeft % 60);
        if (seconds == 60)
        {
            minutes++;
            seconds = 0.0f;
        }
        m_timeText.text = "Zeit: " + minutes.ToString("00") + ":" + seconds.ToString("00");
        m_timeLeft -= Time.deltaTime;
    }

    public void StartCountdown()
    {
        m_countdownText.gameObject.SetActive(true);
        int iCountdown = (int)m_countdown;
        m_countdownText.text = iCountdown.ToString();
        m_countdown -= Time.deltaTime;

        if (m_countdown < 1)
        {
            m_countdownText.text = "Start";
            GameManager.instance.m_bIsRunning = true;
        }
    }

    public void StopTime()
    {
        m_timeLeft = m_timeOriginal;
    }

  
}
