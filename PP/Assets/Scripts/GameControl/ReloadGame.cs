﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadGame : MonoBehaviour {

    internal Button m_newGameButton;
    internal Button m_backToMenuButton;

	// Use this for initialization
	void Start () {
        m_newGameButton = transform.GetChild(0).GetComponent<Button>();
        m_backToMenuButton = transform.GetChild(1).GetComponent<Button>();
        m_newGameButton.onClick.AddListener(CallNewGame);
        m_backToMenuButton.onClick.AddListener(CallBackToMenu);
	}
	
	public void CallNewGame()
    {
        GameManager.instance.NewGame();
    }

    public void CallBackToMenu()
    {
        GameManager.instance.BackToMenu();
    }
}
