﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class NextLevel : MonoBehaviour {

    public GameObject m_levelUpCanvas;
    internal Button m_button;
    internal float m_gregorTimer;
    [SerializeField]
    internal float m_gregorTimerMax;
    //internal bool m_bPlayerPressedNext;

    // Use this for initialization
    void Awake () {
        m_levelUpCanvas.GetComponent<ButtonHighlightLevelUp>().m_eventSystem.SetSelectedGameObject(transform.GetChild(0).gameObject);
        m_gregorTimer = m_gregorTimerMax;
        //m_bPlayerPressedNext = false;
        m_button = transform.GetChild(0).GetComponent<Button>();
        m_button.GetComponent<Image>().color = new Color (75, 0, 130, 180);
        m_button.onClick.AddListener(TaskOnClick);
    }

    //private void Update()
    //{
    //    if (m_bPlayerPressedNext)
    //    {
    //        m_gregorTimer -= Time.deltaTime;
    //        if (m_gregorTimer < 0)
    //        {
    //            GameManager.instance.m_bIsRunning = false;
    //            m_bPlayerPressedNext = false;
    //            GameManager.instance.GetRandomStage();
    //        }
    //    }
    //}

    public void TaskOnClick()
    {
        GameManager.instance.m_bIsRunning = false;
        GameManager.instance.GetRandomStage();
    }
}
