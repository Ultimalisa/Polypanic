﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Rewired;

public class Respawn : MonoBehaviour
{

    internal bool m_didItOnce = false;

    public Text m_Countdown;
    public Text m_doNotSleep;
    public Text m_winnerAnnouncement;
    public Canvas m_Canvas;
    public Canvas m_Draw;
    public Button m_newGameButton;
    public Button m_backToMenuButton;
    public Canvas m_overallWinCanvas;

    public Canvas m_blueWin;
    public Canvas m_redWin;
    public Canvas m_yellowWin;
    public Canvas m_greenWin;

    public GameObject m_gameTimer;

    public float m_timerAfterRound;
    internal bool m_bPlayerHasSelected;
    internal bool m_bTimerIsAllowedToRun;
    public float m_showWinnerTime = 10f;
    internal float m_drawCountdown;
    internal bool m_bSetRandom;
    internal int m_playerAlive;

    internal List<PlayerController> m_pcList;
    internal ButtonHighlightLevelUp m_buttonHighlightLevelUp;
    public UnityEngine.EventSystems.EventSystem m_eventSystem;

    // Use this for initialization
    void Start()
    {
        m_overallWinCanvas.gameObject.SetActive(false);
        //m_winnerAnnouncement.gameObject.SetActive(false);
        m_Countdown.gameObject.SetActive(false);
        m_doNotSleep.gameObject.SetActive(false);

        m_drawCountdown = m_gameTimer.GetComponent<Timer>().m_countdown;
        GameManager.instance.m_bIsRunning = false;

        m_playerAlive = GameManager.instance.GetPlayerNumber();
        m_bPlayerHasSelected = false;
        m_bSetRandom = false;
        m_didItOnce = true;
        m_bTimerIsAllowedToRun = true;
        m_pcList = new List<PlayerController>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
            {
                m_pcList.Add(transform.GetChild(i).GetComponent<PlayerController>());
                transform.GetChild(i).GetComponent<PlayerController>().m_player.isPlaying = true;
            }
            else
                transform.GetChild(i).GetComponent<PlayerController>().m_player.isPlaying = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_playerAlive == 1) //if only one player is left
        {

            int playerID = (int)transform.GetComponentInChildren<PlayerController>().m_playerID + 1;
            Debug.Log(playerID + "playerID win screen");
            if (m_didItOnce == true)
            {
                switch (playerID)
                {
                    case (1):
                        GameManager.instance.m_winPlayerOne++;
                        GameManager.instance.m_playerUIAbove.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_winPlayerOne.ToString();
                        for (int i = 0; i < m_pcList.Count; i++)
                        {
                            if ((int)m_pcList[i].m_playerID != playerID - 1)
                            {
                                m_pcList[i].m_player.isPlaying = false;
                            }
                            else
                                m_pcList[i].m_player.isPlaying = true;
                        }
                        m_didItOnce = false;
                        break;
                    case (2):
                        GameManager.instance.m_winPlayerTwo++;
                        GameManager.instance.m_playerUIAbove.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_winPlayerTwo.ToString();
                        for (int i = 0; i < m_pcList.Count; i++)
                        {
                            if ((int)m_pcList[i].m_playerID != playerID - 1)
                            {
                                m_pcList[i].m_player.isPlaying = false;
                            }
                            else
                                m_pcList[i].m_player.isPlaying = true;
                        }
                        m_didItOnce = false;
                        break;
                    case (3):
                        GameManager.instance.m_winPlayerThree++;
                        GameManager.instance.m_playerUIAbove.transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_winPlayerThree.ToString();
                        for (int i = 0; i < m_pcList.Count; i++)
                        {
                            if ((int)m_pcList[i].m_playerID != playerID - 1)
                            {
                                m_pcList[i].m_player.isPlaying = false;
                            }
                            else
                                m_pcList[i].m_player.isPlaying = true;
                        }
                        m_didItOnce = false;
                        break;
                    case (4):
                        GameManager.instance.m_winPlayerFour++;
                        GameManager.instance.m_playerUIAbove.transform.GetChild(2).transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_winPlayerFour.ToString();
                        for (int i = 0; i < m_pcList.Count; i++)
                        {
                            if ((int)m_pcList[i].m_playerID != playerID - 1)
                            {
                                m_pcList[i].m_player.isPlaying = false;
                            }
                            else
                                m_pcList[i].m_player.isPlaying = true;
                        }
                        m_didItOnce = false;
                        break;
                }
            }

            if (GameManager.instance.m_winPlayerOne == 3 || GameManager.instance.m_winPlayerTwo == 3 || GameManager.instance.m_winPlayerThree == 3 || GameManager.instance.m_winPlayerFour == 3)
            {
                m_gameTimer.GetComponent<Timer>().StopTime();
                m_gameTimer.GetComponent<Timer>().m_timeText.gameObject.SetActive(false);
                GameManager.instance.m_bIsRunning = false;
                m_overallWinCanvas.gameObject.SetActive(true);

                if (GameManager.instance.m_winPlayerOne == 3)
                    m_winnerAnnouncement.text = "The yellow player has won the game!!";

                if (GameManager.instance.m_winPlayerTwo == 3)
                    m_winnerAnnouncement.text = "The blue player has won the game!!";

                if (GameManager.instance.m_winPlayerThree == 3)
                    m_winnerAnnouncement.text = "The green player has won the game!!";

                if (GameManager.instance.m_winPlayerFour == 3)
                    m_winnerAnnouncement.text = "The red player has won the game!!";

                if (m_eventSystem.currentSelectedGameObject == null)
                    m_eventSystem.SetSelectedGameObject(m_newGameButton.gameObject);

                for (int i = 0; i < m_overallWinCanvas.transform.childCount-1; i++) //children of OverallWinCanvas are Buttons and one Text (we only want the buttons)
                {
                    if (m_eventSystem.currentSelectedGameObject == m_overallWinCanvas.transform.GetChild(i).gameObject)
                        m_overallWinCanvas.transform.GetChild(i).GetComponent<ColorLerp>().enabled = true;
                    else
                    {
                        m_overallWinCanvas.transform.GetChild(i).GetComponent<ColorLerp>().enabled = false;
                        m_overallWinCanvas.transform.GetChild(i).GetComponent<Image>().color = new Color (0.5f, 0.5f, 0.5f, 0.5f);
                    }
                }
                m_timerAfterRound = 200f; //random value to keep the timer from influencing the game

            }
            switch (playerID)
            {
                case (4):
                    m_redWin.gameObject.SetActive(true);
                    break;
                case (2):
                    m_blueWin.gameObject.SetActive(true);
                    break;
                case (1):
                    m_yellowWin.gameObject.SetActive(true);
                    break;
                case (3):
                    m_greenWin.gameObject.SetActive(true);
                    break;


            }

            if (m_bTimerIsAllowedToRun)
            {
                m_timerAfterRound -= Time.deltaTime;
                int iTimer = (int)m_timerAfterRound;
                m_Countdown.text = iTimer.ToString();
                if (m_timerAfterRound < m_showWinnerTime)
                {
                    m_redWin.gameObject.SetActive(false);
                    m_blueWin.gameObject.SetActive(false);
                    m_yellowWin.gameObject.SetActive(false);
                    m_greenWin.gameObject.SetActive(false);

                    GameManager.instance.m_playerUIAbove.SetActive(true);

                    m_Canvas.gameObject.SetActive(true);
                }
                if (m_timerAfterRound < 11 && !m_bPlayerHasSelected)
                {
                    m_Countdown.gameObject.SetActive(true);

                }
                if (m_timerAfterRound < 0)
                {
                    m_doNotSleep.gameObject.SetActive(true);
                    m_doNotSleep.text = "Don't Sleep!!";
                    if (!m_bPlayerHasSelected)
                    {
                        m_buttonHighlightLevelUp = FindObjectOfType<ButtonHighlightLevelUp>();
                        if (m_buttonHighlightLevelUp != null)
                            m_buttonHighlightLevelUp.SetRandomStats();
                    }
                    m_Countdown.gameObject.SetActive(false);
                    m_bTimerIsAllowedToRun = false;
                }
            }
            
        }
          
        if (m_playerAlive < 1 || m_gameTimer.GetComponent<Timer>().m_timeLeft < 0)
        {
            m_Draw.gameObject.SetActive(true);
            m_Countdown.gameObject.SetActive(true);
            m_drawCountdown -= Time.deltaTime;
            int iTimer = (int)m_drawCountdown;

            m_Countdown.text = iTimer.ToString();
            if (m_drawCountdown < 1)
                GameManager.instance.GetRandomStage();
        }
    }


}