﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;
using UnityEngine.UI;

public class SpecialNegation : MonoBehaviour {

    public GameObject go1;
    // public Animator animator;
    Image img;
    float m_timer;
    bool m_flash = false;
	// Use this for initialization
	void Start () {

        img = GameObject.Find("PanelFlash").GetComponent<Image>();
        m_timer = 0.5f;
        img.color = new Color(1f, 1f, 1f, 0f);
    }
	
	// Update is called once per frame
	void Update () {
        if (m_flash)
        {
            StartCoroutine(Flash());
           
        }
      
	}
    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if (_collision.gameObject.tag == "player")
        {
            m_flash = true;

            //GameObject.Find("Player1").GetComponent<PlayerController>().m_weaponType = EWeaponType.DEFAULT;
            //GameObject.Find("Player2").GetComponent<PlayerController>().m_weaponType = EWeaponType.DEFAULT;
            //GameObject.Find("Player3").GetComponent<PlayerController>().m_weaponType = EWeaponType.DEFAULT;
            //GameObject.Find("Player4").GetComponent<PlayerController>().m_weaponType = EWeaponType.DEFAULT;
       

            for (int i = 0; i < GameManager.instance.m_playerControllers.Count; i++)
            {
                GameManager.instance.m_playerControllers[i].m_weaponType = EWeaponType.DEFAULT;
                for (int n = 0; n < GameManager.instance.m_playerControllers[i].m_particlesystems.Count; n++)
                {
                    GameManager.instance.m_playerControllers[i].m_particlesystems[n].gameObject.SetActive(false);
                    GameManager.instance.m_playerControllers[i].m_particlesystems[n].enableEmission = false;
                }
                //GameManager.instance.m_playerControllers[i].m_specialPic.sprite = null;

            }

            Destroy(this.gameObject,0.5f);
        }
    }

    IEnumerator Flash()
    {


        img.color = new Color(1f, 1f, 1f, 1f);

        yield return new WaitForSeconds(0.1f);


        img.color = new Color(1f, 1f, 1f, 0f);

        m_flash = false;
    }
    
}
