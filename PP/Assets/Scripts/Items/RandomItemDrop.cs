﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomItemDrop : MonoBehaviour {

    public GameObject m_Element;
    internal Transform m_thisTransform;
    internal int random;
    internal float floatRandom;


   static bool m_dropedCoin = true;
    public ParticleSystem m_ps;

    public GameObject m_rangePlus;
    public GameObject m_speedPlus;

    //public GameObject m_ammoPlus;
    public GameObject m_specialNegation;

    private void Start()
    {
        m_ps = GetComponent<ParticleSystem>();
        m_thisTransform = this.GetComponent<Transform>();
    }

    private void OnCollisionEnter2D(Collision2D _collision)
    {
        if (_collision.gameObject.tag == "bullet")
        {
            random = Random.Range(1, 11); //returns either 1 or 10
            //Debug.Log(random);
            if (random == 2 || random == 8)
            {
                GameObject go1 = (GameObject)Instantiate(m_Element);
                go1.GetComponent<Element>().m_weaponType = (EWeaponType)Random.Range(1, System.Enum.GetValues(typeof(EWeaponType)).Length);  //Setzt das Element der Waffe zufällig           
                //go1.GetComponent<Element>().m_weaponLevel = EWeaponLevel.ONE;
                go1.GetComponent<Element>().SetSprite();
                go1.transform.position = m_thisTransform.position;
            }
            else if (random == 4 || random == 6)
            {
                floatRandom = Random.Range(-1f, 1f);
                if (floatRandom < 0f && floatRandom > -0.8f)
                {
                    GameObject range = (GameObject)Instantiate(m_rangePlus);
                    range.transform.position = m_thisTransform.position;
                }
                if (floatRandom > 0f)
                {
                    GameObject reload = (GameObject)Instantiate(m_speedPlus);
                    reload.transform.position = m_thisTransform.position;
                }
                //else
                //{
                //    GameObject ammo = (GameObject)Instantiate(m_ammoPlus);
                //    ammo.transform.position = m_thisTransform.position;
                //}
                if(floatRandom < -0.8f)
                {
                    GameObject negation = (GameObject)Instantiate(m_specialNegation);
                    negation.transform.position = m_thisTransform.position;
                }
            }

            if (random == 1)
            {
                floatRandom = Random.Range(-1f, 1f);
                if(floatRandom < -0.5f && m_dropedCoin == true)
                {
                    GameObject coin = (GameObject)Instantiate(GameManager.instance.m_coin);
                    coin.transform.position = m_thisTransform.position;
                    m_dropedCoin = false;
                }
            }
            m_ps.Play();
            Destroy(this.gameObject);

            
            

        }
    }

    public void SpawnItem()
    {
        int random = Random.Range(1, 11); //returns either -1 or 10
                                      //Debug.Log(random);
        if (random == 2 || random == 8)
        {
            GameObject go1 = (GameObject)Instantiate(m_Element);
            go1.GetComponent<Element>().m_weaponType = (EWeaponType)Random.Range(1, System.Enum.GetValues(typeof(EWeaponType)).Length);  //Setzt das Element der Waffe zufällig           
            //go1.GetComponent<Element>().m_weaponLevel = EWeaponLevel.ONE;
            go1.GetComponent<Element>().SetSprite();
            go1.transform.position = m_thisTransform.position;
        }
        else if (random == 4 || random == 6)
        {
            floatRandom = Random.Range(-1f, 1f);
            if (floatRandom < 0f && floatRandom >  -0.9f)
            {
                GameObject range = (GameObject)Instantiate(m_rangePlus);
                range.transform.position = m_thisTransform.position;
            }
            if (floatRandom > 0f)
            {
                GameObject reload = (GameObject)Instantiate(m_speedPlus);
                reload.transform.position = m_thisTransform.position;
            }
            if (floatRandom < -0.8f)
            {
                GameObject negation = (GameObject)Instantiate(m_specialNegation);
               negation.transform.position = m_thisTransform.position;
            }
            //else
            //{
            //    GameObject ammo = (GameObject)Instantiate(m_ammoPlus);
            //    ammo.transform.position = m_thisTransform.position;
            //}
        }


        if (random == 1)
        {
            floatRandom = Random.Range(-1f, 1f);
            if (floatRandom < -0.3f && m_dropedCoin == true)
            {
                GameObject coin = (GameObject)Instantiate(GameManager.instance.m_coin);
                coin.transform.position = m_thisTransform.position;
                m_dropedCoin = false;
            }

        }
    }
}
