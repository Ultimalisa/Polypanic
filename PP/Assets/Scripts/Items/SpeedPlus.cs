﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPlus : MonoBehaviour {

    [SerializeField]
    protected float m_speedMax;

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if (_collision.gameObject.tag == "player")
        {
            PlayerController pc = _collision.gameObject.GetComponent<PlayerController>();
            if (pc.m_standardSpeedOrigin < m_speedMax)
            {
                pc.m_standardSpeed += 50;
                pc.m_standardSpeedOrigin += 50;
                if (pc.m_standardSpeedOrigin == m_speedMax)
                    pc.m_speedCountText.text = "Max";
                else
                {
                    pc.m_speedPlusCounter++;
                    pc.m_speedCountText.text = pc.m_speedPlusCounter.ToString();
                }
            }

            Destroy(this.gameObject);
        }
    }
}
