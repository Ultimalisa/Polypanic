﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangePlus : MonoBehaviour {

    [SerializeField]
    protected float m_maxRange;

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if (_collision.gameObject.tag == "player")
        {
            PlayerController pc = _collision.gameObject.GetComponent<PlayerController>();
            if (pc.m_standardRangeOrigin < m_maxRange)
            {
                pc.m_standardRange += 100;
                pc.m_standardRangeOrigin += 100;
                if (pc.m_standardRangeOrigin == m_maxRange)
                    pc.m_rangeCountText.text = "Max";
                else
                {
                    pc.m_rangePlusCounter++;
                    pc.m_rangeCountText.text = pc.m_rangePlusCounter.ToString();
                }
            }

            Destroy(this.gameObject);
        }
    }
}
