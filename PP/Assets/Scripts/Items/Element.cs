﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EWeaponType
{
    DEFAULT,
    WATER,
    FIRE,
    LIGHTNING
}

public enum EWeaponLevel
{
    ZERO,
    ONE,
    TWO,
    THREE,
    FOUR
}

public class Element : MonoBehaviour {

    [SerializeField]
    internal float m_Lifetime;
    [SerializeField]
    internal List<Sprite> m_spriteList;

    public EWeaponType m_weaponType;
    //public EWeaponLevel m_weaponLevel;

    private void Awake()
    {
        m_weaponType = EWeaponType.DEFAULT;
        //m_weaponLevel = EWeaponLevel.ZERO;
    }

    // Update is called once per frame
    void Update () {

        m_Lifetime -= Time.deltaTime;   //Despawn
                                        //
        if (m_Lifetime < 0)             //
            Destroy(this.gameObject);   //
	}

    public void SetSprite()
    {
        switch (m_weaponType)
        {
            case EWeaponType.WATER:
                this.GetComponent<SpriteRenderer>().sprite = m_spriteList[0];
                break;
            case EWeaponType.FIRE:
                this.GetComponent<SpriteRenderer>().sprite = m_spriteList[1];
                break;
            case EWeaponType.LIGHTNING:
                this.GetComponent<SpriteRenderer>().sprite = m_spriteList[2];
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if(_collision.gameObject.tag == "player")
        {
            //Set the weapon type and level to this weapon
            _collision.GetComponent<PlayerController>().m_weaponType = this.m_weaponType;

            switch (m_weaponType)
            {
                case EWeaponType.FIRE:
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[0].gameObject.SetActive(true);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[0].enableEmission = true;
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[1].gameObject.SetActive(false);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[1].enableEmission = false;
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[2].gameObject.SetActive(false);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[2].enableEmission = false;
                    break;
                case EWeaponType.LIGHTNING:
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[1].gameObject.SetActive(true);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[1].enableEmission = true;
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[0].gameObject.SetActive(false);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[0].enableEmission = false;
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[2].gameObject.SetActive(false);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[2].enableEmission = false;
                    break;
                case EWeaponType.WATER:
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[2].gameObject.SetActive(true);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[2].enableEmission = true;
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[0].gameObject.SetActive(false);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[0].enableEmission = false;
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[1].gameObject.SetActive(false);
                    _collision.gameObject.GetComponent<PlayerController>().m_particlesystems[1].enableEmission = false;
                    break;
            }

            //_collision.GetComponent<PlayerController>().m_weaponLevel = this.m_weaponLevel;
            //_collision.GetComponent<PlayerController>().m_specialPic.sprite = this.GetComponent<SpriteRenderer>().sprite;
            Destroy(this.gameObject); //Destroy this Item with a Delay of 0.5 seconds
        }
    }
}
