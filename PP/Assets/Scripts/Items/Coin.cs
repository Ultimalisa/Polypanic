﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coin : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if (_collision.gameObject.tag == "player")
        {
            _collision.GetComponent<PlayerController>().m_bHasCoin = true;
            _collision.GetComponent<PlayerController>().m_healthBarCanvas.transform.GetChild(2).GetComponent<Image>().sprite = GameManager.instance.m_coinSprite;
            _collision.GetComponent<PlayerController>().m_healthBarCanvas.transform.GetChild(2).gameObject.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}
