﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCrystals : MonoBehaviour {

    [SerializeField]
    internal int m_maxCountCrystals;
    [SerializeField]

    List<GameObject> Crystals = new List<GameObject>();
   public GameObject m_Crystal1;
   public GameObject m_Crystal2;
    public GameObject m_Crystal3;
    public GameObject m_Crystal4;
    public GameObject m_Crystal5;
    public GameObject m_Crystal6;

    //Sprite m_random;

    // public Sprite[] spriteList;


    private void Awake()
    {
        Crystals.Add(m_Crystal1);
        Crystals.Add(m_Crystal2);
        Crystals.Add(m_Crystal3);
        Crystals.Add(m_Crystal4);
        Crystals.Add(m_Crystal5);
        Crystals.Add(m_Crystal6);
    }
    // Update is called once per frame
    void Update () {
        //m_random = spriteList[Random.Range(1, 6)];
        //m_Crystal.GetComponent<SpriteRenderer>().sprite = m_random;

        if (this.transform.childCount < m_maxCountCrystals)
        {
            //Debug.Log(transform.childCount);
            foreach (var item in Crystals)
            {
                item.gameObject.GetComponent<ParticleSystem>().Stop();
            }

           

            GameObject go = (GameObject)Instantiate(Crystals[Random.Range((int)1,(int)6)], this.transform);

           
            go.transform.localPosition = new Vector2(Random.Range(-890, 891), Random.Range(-470, 471));
        }

	}
}
