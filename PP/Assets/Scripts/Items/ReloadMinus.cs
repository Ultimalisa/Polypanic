﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadMinus : MonoBehaviour {

    [SerializeField]
    internal float m_reloadMin;
    [SerializeField]
    internal float m_reloadMinus;

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if (_collision.gameObject.tag == "player")
        {
            if (_collision.gameObject.GetComponent<PlayerController>().m_reloadMax > m_reloadMin)
                _collision.gameObject.GetComponent<PlayerController>().m_reloadMax -= m_reloadMinus;
            //else if (_collision.gameObject.GetComponent<PlayerController>().m_reloadMax == 1f)
            //    _collision.gameObject.GetComponent<PlayerController>().m_reloadMax -= m_reloadMin;

            Destroy(this.gameObject);
        }
    }
}
