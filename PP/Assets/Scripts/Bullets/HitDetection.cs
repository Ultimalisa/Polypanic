﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitDetection : MonoBehaviour {

   
    public EPlayerID bulletID;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnCollisionEnter2D(Collision2D _collision)
    {

        //Debug.Log(_collision.gameObject.tag);

        if ((_collision.gameObject.tag == "player") && (bulletID != _collision.gameObject.GetComponent<PlayerController>().m_playerID) && !_collision.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable)
        {

            _collision.gameObject.GetComponent<Health>().Damage(1);
            _collision.gameObject.GetComponent<PlayerController>().m_bIsInvulnerable = true;
            Destroy(this.gameObject);
        }
        else if ((_collision.gameObject.tag == "player") && (bulletID == _collision.gameObject.GetComponent<PlayerController>().m_playerID))
            return;
        else
            Destroy(this.gameObject);
    }
}
