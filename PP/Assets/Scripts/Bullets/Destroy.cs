﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{

    public float delay = 5f;

    private void Start()
    {
        Destroy(this.gameObject, delay);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "stone")
        {
            Destroy(this.gameObject);
        }
    }
}