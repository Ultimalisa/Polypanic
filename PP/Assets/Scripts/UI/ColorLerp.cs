﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EColor
{
    ORANGE,
    PURPLE,
    BLUE,
    Transparent
}

public class ColorLerp : MonoBehaviour {

    public EColor m_eColor;

    internal Image m_image;
    [SerializeField]
    internal float m_lerpSpeed;

    private Color m_color;
    internal Color m_originalColor;

	// Use this for initialization
	void Awake () {
        m_image = GetComponent<Image>();
        m_originalColor = m_image.color;
        switch (m_eColor)
        {
            case EColor.ORANGE:
                m_color = new Color(1f, 0.6f, 0.2f, 1f);
                break;
            case EColor.PURPLE:
                m_color = new Color(0.25f, 0f, 0.5f, 1f);
                break;
            case EColor.BLUE:
                m_color = new Color(0f, 0.2f, 0.8f, 1f);
                break;
            case EColor.Transparent:
                m_color = m_image.color;
                break;
        }
         //m_image.color = new Color(1, 0.6f, 0.2f, 1);
    }
	
	// Update is called once per frame
	void Update () {
        m_image.color = new Color(m_color.r, m_color.g, m_color.b, Mathf.PingPong(Time.time *m_lerpSpeed, 1));
	}

    private void OnEnable()
    {
        m_image.color = new Color(m_color.r, m_color.g, m_color.b, 1f);
    }
}
