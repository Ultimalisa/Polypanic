﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackToMainMenu : MonoBehaviour {

    public Canvas m_playerSelect;
    public Canvas m_mainMenu;
    public Button m_playButton;

    public UnityEngine.EventSystems.EventSystem m_eventSystem;
    internal Button m_button;

	// Use this for initialization
	void Start () {
        m_button = GetComponent<Button>();
        m_button.onClick.AddListener(BackToMenu);
	}

    public void BackToMenu()
    {
        m_mainMenu.gameObject.SetActive(true);
        m_eventSystem.SetSelectedGameObject(m_playButton.gameObject);
        m_playerSelect.gameObject.SetActive(false);
    }
}
