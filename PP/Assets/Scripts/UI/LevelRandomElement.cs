﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelRandomElement : MonoBehaviour {

    internal Button m_button;
    private ButtonHighlightLevelUp m_buttonHighlightLevelUp;
    public List<Button> m_notMaxedButtons;

	// Use this for initialization
	void Start () {
        m_buttonHighlightLevelUp = transform.parent.transform.parent.GetComponent<ButtonHighlightLevelUp>();
        m_buttonHighlightLevelUp.m_levelRandomElementList.Add(this);
        m_button = GetComponent<Button>();
        m_notMaxedButtons = new List<Button>();
        for (int i = 0; i < transform.parent.transform.childCount-1; i++)
        {
            if (transform.parent.transform.GetChild(i).GetComponent<Button>().enabled)
                m_notMaxedButtons.Add(transform.parent.transform.GetChild(i).GetComponent<Button>());
        }
        //m_button.enabled = false;
        m_button.onClick.AddListener(SetRandomState);
    }

    public void SetRandomState()
    {
        if (m_notMaxedButtons.Count > 0)
        {
            int random = Random.Range(0, m_notMaxedButtons.Count);
            transform.parent.transform.GetChild(random).GetComponent<ButtonClick>().TaskOnClick();
        }
        else
        {
            transform.parent.transform.GetChild(0).GetComponent<ButtonClick>().TaskOnClick();
        }
            
        m_button.enabled = false;
    }
}
