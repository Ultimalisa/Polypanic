﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeButtonName : MonoBehaviour {

    internal int m_value = 1;
    internal Text m_text;
    EWeaponType m_type;
    EPlayerID m_ID;

    private Color m_maxColor;

    internal ButtonHighlightLevelUp m_buttonHighlightLevelUp;

    private void Awake()
    {
        m_type = transform.parent.GetComponent<ButtonClick>().m_eWeaponType;
        m_ID = transform.parent.GetComponent<ButtonClick>().m_ePlayerID;

        m_buttonHighlightLevelUp = transform.parent.transform.parent.transform.parent.GetComponent<ButtonHighlightLevelUp>();
        m_maxColor = m_buttonHighlightLevelUp.m_colorOfMaxButtons;

        switch (m_ID)
        {
            case EPlayerID.Player1:
                switch (m_type)
                {
                    case EWeaponType.WATER:
                        m_value = GameManager.instance.m_currentWaterLevel1;
                        break;
                    case EWeaponType.FIRE:
                        m_value = GameManager.instance.m_currentFireLevel1;
                        break;
                    case EWeaponType.LIGHTNING:
                        m_value = GameManager.instance.m_currentLightningLevel1;
                        break;
                }
                break;
            case EPlayerID.Player2:
                switch (m_type)
                {
                    case EWeaponType.WATER:
                        m_value = GameManager.instance.m_currentWaterLevel2;
                        break;
                    case EWeaponType.FIRE:
                        m_value = GameManager.instance.m_currentFireLevel2;
                        break;
                    case EWeaponType.LIGHTNING:
                        m_value = GameManager.instance.m_currentLightningLevel2;
                        break;
                }
                break;
            case EPlayerID.Player3:
                switch (m_type)
                {
                    case EWeaponType.WATER:
                        m_value = GameManager.instance.m_currentWaterLevel3;
                        break;
                    case EWeaponType.FIRE:
                        m_value = GameManager.instance.m_currentFireLevel3;
                        break;
                    case EWeaponType.LIGHTNING:
                        m_value = GameManager.instance.m_currentLightningLevel3;
                        break;
                }
                break;
            case EPlayerID.Player4:
                switch (m_type)
                {
                    case EWeaponType.WATER:
                        m_value = GameManager.instance.m_currentWaterLevel4;
                        break;
                    case EWeaponType.FIRE:
                        m_value = GameManager.instance.m_currentFireLevel4;
                        break;
                    case EWeaponType.LIGHTNING:
                        m_value = GameManager.instance.m_currentLightningLevel4;
                        break;
                }
                break;
        }

        m_text = GetComponent<Text>();
        if (m_value < GameManager.instance.m_maxLevel)
        {
            m_text.text += m_value;
        }
        else if (m_value >= GameManager.instance.m_maxLevel)
        {
            m_text.text = m_text.text.Substring(0, m_text.text.Length - 1) + "MAX";
            transform.parent.GetComponent<Image>().color = m_maxColor;   //Max Button Color
            transform.parent.GetComponent<Button>().enabled = false;
            
        }

        m_buttonHighlightLevelUp.SelectAButton();
    }

    public void LevelTextUp()
    {
        m_value++;
        if (m_value < GameManager.instance.m_maxLevel)
        {
            m_text.text = m_text.text.Substring(0, m_text.text.Length - 1) + m_value;
        }

        else if (m_value == GameManager.instance.m_maxLevel)
        {
            m_text.text = m_text.text.Substring(0, m_text.text.Length - 2) + "MAX";
        }
        //else
        //    m_text.text = m_text.text;
    }
}
