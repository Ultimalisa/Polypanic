﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;


public class ButtonHighlightLevelUp : MonoBehaviour {

    public UnityEngine.EventSystems.EventSystem m_eventSystem;
    //public GameObject m_firstSelectedObject;
    //public GameObject m_secondSelectedObject;
    //public GameObject m_thirdSelectedObject;
    //public GameObject m_fourthSelectedObject;
    public Canvas m_nextLevelCanvas;

    public List<Button> m_buttonList;
    public List<GameObject> m_panelList;
    internal int m_currentPlayerPanel = 0;
    public List<LevelRandomElement> m_levelRandomElementList;
    public Color m_colorOfDisabledButtons;
    public Color m_colorOfMaxButtons;
    public Color m_colorOfSelectedButtonFromPlayer;

    int playerID;
	// Use this for initialization
	void Awake () {
        GameManager.instance.m_bIsRunning = false;
        m_levelRandomElementList = new List<LevelRandomElement>();
        m_buttonList = new List<Button>();
        m_panelList = new List<GameObject>();
        playerID = (int)GameObject.Find("Players").transform.GetComponentInChildren<PlayerController>().m_playerID+1;
        Debug.Log(playerID + "PlayerID");

        for (int i = 1; i < transform.childCount+1; i++)
        {
            if (i > GameManager.instance.GetPlayerNumber())
                transform.GetChild(i-1).gameObject.SetActive(false);
            else
            {
                for (int n = 0; n < transform.childCount; n++)
                {
                    m_buttonList.Add(GameObject.Find("Panel" + i).transform.GetChild(n).GetComponent<Button>());
                }
            }
        }

        

        //Disable all element Buttons
        for (int i = 0; i < m_buttonList.Count; i++)
        {
            m_buttonList[i].transform.GetComponent<Image>().color = m_colorOfDisabledButtons; //Color of disabled Buttons
            m_buttonList[i].enabled = false;
        }

        //Set the LevelCanvas of the winner to inactive only if winner hasn't picked up the coin
        switch (playerID)
        {
            case (1):
                if (!GameManager.instance.m_playerControllers[0].m_bHasCoin)
                    transform.GetChild(0).gameObject.SetActive(false);
                //m_eventSystem.SetSelectedGameObject(m_firstSelectedObject);
                break;
            case (2):
                if (!GameManager.instance.m_playerControllers[1].m_bHasCoin)
                    transform.GetChild(1).gameObject.SetActive(false);
                //m_eventSystem.SetSelectedGameObject(m_secondSelectedObject);
                break;
            case (3):
                if (!GameManager.instance.m_playerControllers[2].m_bHasCoin)
                    transform.GetChild(2).gameObject.SetActive(false);
                //m_eventSystem.SetSelectedGameObject(m_thirdSelectedObject);
                break;
            case (4):
                if (!GameManager.instance.m_playerControllers[3].m_bHasCoin)
                    transform.GetChild(3).gameObject.SetActive(false);
                //m_eventSystem.SetSelectedGameObject(m_fourthSelectedObject);
                break;
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
                m_panelList.Add(transform.GetChild(i).gameObject);
        }

        EnableButtons();
        SelectAButton();
    }


    public void CheckPlayerPanels()
    {
        DisableButtons();
        m_panelList[m_currentPlayerPanel].GetComponent<Image>().color = Color.white;
        m_panelList[m_currentPlayerPanel].GetComponent<ColorLerp>().enabled = false;

        m_currentPlayerPanel++;
        if (m_currentPlayerPanel < m_panelList.Count)
        {
            EnableButtons();
            DisableMaxButtons();
            SelectAButton();
        }
        else
        {
            m_nextLevelCanvas.gameObject.SetActive(true);
            //FindObjectOfType<Respawn>().m_bPlayerHasSelected = true; //don't let the timer select a random Button
        }
    }

    public void DisableButtons()
    {
        for (int i = 0; i < m_buttonList.Count; i++)
        {
            if (m_buttonList[i].isActiveAndEnabled) //Disable and color only active Buttons
            {
                m_buttonList[i].image.color = m_colorOfDisabledButtons;   //Color of disabled Buttons
                m_buttonList[i].enabled = false;
            }
        }
    }

    public void DisableMaxButtons()
    {
        for (int i = 0; i < m_panelList[m_currentPlayerPanel].transform.childCount-1; i++)
        {
            if (m_panelList[m_currentPlayerPanel].transform.GetChild(i).transform.GetChild(0).GetComponent<ChangeButtonName>().m_value >= GameManager.instance.m_maxLevel)
            {
                m_panelList[m_currentPlayerPanel].transform.GetChild(i).GetComponent<Button>().image.color = m_colorOfMaxButtons; //Max Color
                m_panelList[m_currentPlayerPanel].transform.GetChild(i).GetComponent<Button>().enabled = false;
                
            }
        }
    }

    public void EnableButtons()
    {
        for (int i = 0; i < m_panelList[m_currentPlayerPanel].transform.childCount; i++)
        {
            m_panelList[m_currentPlayerPanel].transform.GetChild(i).GetComponent<Button>().enabled = true;
            m_panelList[m_currentPlayerPanel].transform.GetChild(i).GetComponent<Button>().image.color = Color.white;
        }

        //Select Button
        //m_eventSystem.SetSelectedGameObject(m_panelList[m_currentPlayerPanel].transform.GetChild(0).gameObject);

        //Disable Max Buttons

        //SelectAButton();

        m_panelList[m_currentPlayerPanel].GetComponent<ColorLerp>().enabled = true;
    }

    public void SelectAButton()
    {
        for (int i = 0; i < m_panelList[m_currentPlayerPanel].transform.childCount; i++)
        {
            if (m_panelList[m_currentPlayerPanel].transform.GetChild(i).GetComponent<Button>().isActiveAndEnabled)
            {
                m_eventSystem.SetSelectedGameObject(m_panelList[m_currentPlayerPanel].transform.GetChild(i).gameObject);
                break;
            }
        }
    }

    public void SetRandomStats()
    {
        for (int i = 0; i < m_levelRandomElementList.Count; i++)
            m_levelRandomElementList[i].SetRandomState();
    }

}
