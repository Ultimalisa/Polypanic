﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClick : MonoBehaviour {

    private Button m_button;

    public EPlayerID m_ePlayerID;
    public EWeaponType m_eWeaponType;

    internal Image m_image;

    private ButtonHighlightLevelUp m_buttonHighlightLevelUp;
    private Color m_selectedColor;

    // Use this for initialization
    void Start () {
        m_buttonHighlightLevelUp = transform.parent.transform.parent.GetComponent<ButtonHighlightLevelUp>();
        m_selectedColor = m_buttonHighlightLevelUp.m_colorOfSelectedButtonFromPlayer;
        m_button = GetComponent<Button>();
        m_image = GetComponent<Image>();
        //m_button.enabled = false;
        m_button.onClick.AddListener(TaskOnClick);
        //m_button.onClick.AddListener(RemoveThisButton);
    }
	
	public void TaskOnClick()
    {
        GameManager.instance.LevelUP(m_ePlayerID, m_eWeaponType);
        //m_button.GetComponent<Button>().image.color = Color.grey;
        m_image.color = m_selectedColor; // Color of selected Button
        m_buttonHighlightLevelUp.m_levelRandomElementList.Remove(transform.parent.GetComponentInChildren<LevelRandomElement>());
        m_button.enabled = false;
        transform.GetChild(0).GetComponent<ChangeButtonName>().LevelTextUp();
        transform.parent.transform.parent.GetComponent<ButtonHighlightLevelUp>().CheckPlayerPanels();
    }

    //[deprecated]
    public void RemoveThisButton()
    {
        for (int i = 0; i < m_buttonHighlightLevelUp.m_buttonList.Count; i++)
            if (m_buttonHighlightLevelUp.m_buttonList[i].gameObject == this.gameObject)
                m_buttonHighlightLevelUp.m_buttonList.Remove(this.m_button);
    }
}
