﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackToMenu : MonoBehaviour {

    internal Button m_button;
    

	// Use this for initialization
	void Start () {
        m_button = GetComponent<Button>();
        m_button.onClick.AddListener(CallBackToMenu);
        
	}
	
    public void CallBackToMenu()
    {
        GameManager.instance.BackToMenu();
    }

}
