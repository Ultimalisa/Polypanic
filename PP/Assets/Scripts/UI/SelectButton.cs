﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectButton : MonoBehaviour {

    private UnityEngine.EventSystems.EventSystem m_eventSystem;
    public List<GameObject> m_enableButtonList;
    //private PlayerController m_pc;
    //private Button m_button;

    // Use this for initialization
    void Start () {
        m_eventSystem = transform.parent.GetComponent<ButtonHighlightLevelUp>().m_eventSystem;
        //m_pc = FindObjectOfType<PlayerController>();
        //m_button = GetComponent<Button>();
        //m_button.onClick.AddListener(EnableButtons);
        //m_button.onClick.AddListener(DisablePanels);
        m_enableButtonList = new List<GameObject>();
        for (int i = 0; i < transform.childCount; i++)
        {
            m_enableButtonList.Add(transform.GetChild(i).gameObject);
        }
    }

    //private void Update()
    //{
    //    if (m_pc.m_player.GetButtonDown("Cancel"))
    //    {
    //        GoBack();
    //    }
    //}

  

    //public void DisablePanels()
    //{
    //    for (int i = 0; i < transform.parent.childCount; i++)
    //    {
    //        //if (transform.parent.GetChild(i).gameObject != this.gameObject)
    //            transform.parent.GetChild(i).GetComponent<Button>().enabled = false;
    //    }
    //}

    public void EnableButtons()
    {
        for (int i = 0; i < m_enableButtonList.Count; i++)
        {
            m_enableButtonList[i].GetComponent<Button>().enabled = true;
            SelectAButton();
        }
    }

    //[deprecated]
    public void GoBack()
    {
        for (int i = 0; i < transform.parent.childCount; i++)
        {
            transform.parent.GetChild(i).GetComponent<Button>().enabled = true;
        }
        transform.parent.GetComponent<ButtonHighlightLevelUp>().DisableButtons();
        m_eventSystem.SetSelectedGameObject(this.gameObject);
    }

    public void SelectAButton()
    {
        for (int i = 0; i < m_enableButtonList.Count; i++)
        {
            if (m_enableButtonList[i].GetComponent<Button>().isActiveAndEnabled)
            {
                m_eventSystem.SetSelectedGameObject(m_enableButtonList[i]);
                break;
            }
        }
    }
}
