
a_fire_W.png
size: 256,128
format: RGBA8888
filter: Linear,Linear
repeat: none
fire_ball
  rotate: false
  xy: 141, 16
  size: 108, 75
  orig: 108, 75
  offset: 0, 0
  index: -1
fire_effect
  rotate: false
  xy: 2, 2
  size: 137, 89
  orig: 137, 89
  offset: 0, 0
  index: -1
