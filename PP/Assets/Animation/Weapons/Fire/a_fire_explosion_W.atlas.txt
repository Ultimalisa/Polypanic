
a_fire_explosion_W.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Explosion_fizzles
  rotate: false
  xy: 395, 156
  size: 332, 295
  orig: 332, 295
  offset: 0, 0
  index: -1
big_Explosion
  rotate: false
  xy: 2, 100
  size: 391, 351
  orig: 391, 351
  offset: 0, 0
  index: -1
middle_Explosion
  rotate: false
  xy: 729, 236
  size: 214, 215
  orig: 214, 215
  offset: 0, 0
  index: -1
small_Explosion
  rotate: false
  xy: 2, 2
  size: 93, 96
  orig: 93, 96
  offset: 0, 0
  index: -1
