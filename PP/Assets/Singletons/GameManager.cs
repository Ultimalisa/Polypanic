﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;

    internal bool m_bIsRunning = false;

    public WeaponElement m_weaponElement;
    public VolumeChange m_musicManager;

    public GameObject m_coin;
    public Sprite m_coinSprite;

    private Player m_player;

    [SerializeField]
    private int m_playerNumber;
    public List<PlayerController> m_playerControllers;

    internal int m_currentWaterLevel1;
    internal int m_currentFireLevel1;
    internal int m_currentLightningLevel1;
    internal int m_currentWaterLevel2;
    internal int m_currentFireLevel2;
    internal int m_currentLightningLevel2;
    internal int m_currentWaterLevel3;
    internal int m_currentFireLevel3;
    internal int m_currentLightningLevel3;
    internal int m_currentWaterLevel4;
    internal int m_currentFireLevel4;
    internal int m_currentLightningLevel4;

    public int m_winPlayerOne;
    public int m_winPlayerTwo;
    public int m_winPlayerThree;
    public int m_winPlayerFour;

    internal int m_maxLevel;

    [SerializeField]
    internal GameObject m_playerUIAbove;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        m_currentWaterLevel1 = 1;
        m_currentFireLevel1 = 1;
        m_currentLightningLevel1 = 1;
        m_currentWaterLevel2 = 1;
        m_currentFireLevel2 = 1;
        m_currentLightningLevel2 = 1;
        m_currentWaterLevel3 = 1;
        m_currentFireLevel3 = 1;
        m_currentLightningLevel3 = 1;
        m_currentWaterLevel4 = 1;
        m_currentFireLevel4 = 1;
        m_currentLightningLevel4 = 1;

        m_maxLevel = 4;

        m_playerNumber = 4;

        m_winPlayerOne = 0;
        m_winPlayerTwo = 0;
        m_winPlayerThree = 0;
        m_winPlayerFour = 0;

        m_playerControllers = new List<PlayerController>();
    }

    public void LevelUP(EPlayerID _playerID, EWeaponType _type)
    {

            switch (_playerID)
            {
                case EPlayerID.Player1:

                    switch (_type)
                    {
                        case EWeaponType.WATER:
                            m_currentWaterLevel1++;
                            break;
                        case EWeaponType.FIRE:
                            m_currentFireLevel1++;
                            break;
                        case EWeaponType.LIGHTNING:
                            m_currentLightningLevel1++;
                            break;
                    }
                    break;

                case EPlayerID.Player2:
                    switch (_type)
                    {
                        case EWeaponType.WATER:
                            m_currentWaterLevel2++;
                            break;
                        case EWeaponType.FIRE:
                            m_currentFireLevel2++;
                            break;
                        case EWeaponType.LIGHTNING:
                            m_currentLightningLevel2++;
                            break;
                    }
                    break;

                case EPlayerID.Player3:
                    switch (_type)
                    {
                        case EWeaponType.WATER:
                            m_currentWaterLevel3++;
                            break;
                        case EWeaponType.FIRE:
                            m_currentFireLevel3++;
                            break;
                        case EWeaponType.LIGHTNING:
                            m_currentLightningLevel3++;
                            break;
                    }
                    break;

                case EPlayerID.Player4:
                    switch (_type)
                    {
                        case EWeaponType.WATER:
                            m_currentWaterLevel4++;
                            break;
                        case EWeaponType.FIRE:
                            m_currentFireLevel4++;
                            break;
                        case EWeaponType.LIGHTNING:
                            m_currentLightningLevel4++;
                            break;
                    }
                    break;

            } 
    }

    public void Reset()
    {
        m_currentWaterLevel1 = 1;
        m_currentFireLevel1 = 1;
        m_currentLightningLevel1 = 1;
        m_currentWaterLevel2 = 1;
        m_currentFireLevel2 = 1;
        m_currentLightningLevel2 = 1;
        m_currentWaterLevel3 = 1;
        m_currentFireLevel3 = 1;
        m_currentLightningLevel3 = 1;
        m_currentWaterLevel4 = 1;
        m_currentFireLevel4 = 1;
        m_currentLightningLevel4 = 1;

        m_winPlayerOne = 0;
        m_winPlayerTwo = 0;
        m_winPlayerThree = 0;
        m_winPlayerFour = 0;

        m_bIsRunning = false;
    }

    //[deprecated]
    public void LoadNextScene()
    {
        m_playerControllers.Clear();
        SceneManager.LoadScene(1/*SceneManager.GetActiveScene().buildIndex + 1*/);
    }

    public void SetPlayerNumber(int _value)
    {
        m_playerNumber = _value;
    }

    public int GetPlayerNumber()
    {
        return m_playerNumber;
    }

    public void NewGame()
    {
        Reset();
        GetRandomStage();
    }

    public void BackToMenu()
    {
        Reset();
        m_player = ReInput.players.GetPlayer(0);
        m_player.isPlaying = true;
        m_playerUIAbove = null;
        SceneManager.LoadScene(0);
    }

    public void GetRandomStage()
    {
        m_playerControllers = new List<PlayerController>();
        int random = Random.Range(1, SceneManager.sceneCountInBuildSettings - 1);
        SceneManager.LoadScene(random);
    }
}
