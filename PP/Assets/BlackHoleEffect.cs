﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BlackHoleEffect : MonoBehaviour {

    //public settings

    public Shader shader;
    public Transform blackHole;
    public float ratio; // aspect ratio of the screen
    public float radius; // size of blackhole in scene

    //private settings

    Camera cam;
    Material _material; // will be procedurally generated;

    Timer m_timer;

   

   

    Material material
    {
        get
        {
            if(_material == null)
            {
                _material = new Material(shader);
                _material.hideFlags = HideFlags.HideAndDontSave;
            }
            return _material;
        }
    }


     void OnEnable()
    {
        cam = GetComponent<Camera>();
        ratio = 1f / cam.aspect;
    }

   void OnDisable()
    {
        if (_material)
        {
            DestroyImmediate(_material);
        }
    }

    Vector3 wtsp;
    Vector2 pos;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        {
            if (shader && material && blackHole)
            {
                wtsp = cam.WorldToScreenPoint(blackHole.position);

                //is the blackhole in front of the camera
                if(wtsp.z > 0)
                {
                    pos = new Vector2(wtsp.x / cam.pixelWidth,1- ( wtsp.y / cam.pixelHeight));
                    //apply shader parameters
                    _material.SetVector("_Position",pos);
                    _material.SetFloat("_Ratio", ratio);
                    _material.SetFloat("_Rad", radius);
                    _material.SetFloat("_Distance", Vector3.Distance(blackHole.position, transform.position));

                    //apply the shader

                    Graphics.Blit(source,destination, material);

                }
            }

        }
    }
}
