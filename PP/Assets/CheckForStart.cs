﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckForStart : MonoBehaviour {

    public List<BlackHoleEffect> m_blackholList;
    Timer m_timer;
    private void Start()
    {
        m_timer = FindObjectOfType<Timer>();
    }

    private void Update()
    {
        if (m_timer.m_bNewRoundStarted == false)
        {
            foreach (var item in m_blackholList)
            {
                item.enabled = true;
                
            }
            
        }
    }
}
