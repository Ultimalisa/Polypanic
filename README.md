Projektphase I/III

this is a local multiplayer vs. shooter game for 2-4 players

**mechanics:**


*  each of the players controls a character

*  you use the left analog stick to control your shooting direction. Press-hold the
shoot button to increase the shooting range and release it to shoot

*  you move by using the recoil

*  destroy asteroids and collect items in order to gain buffs or collect elements
to gain a special ability

*  level up your opponents in order to increase the level of difficulty after each
round

*  once a player has three wins they have won the game

**interesting to look at:**

*  player controls can be viewed in /Assets/Scripts/Player/PlayerController.cs

*  the dangerous and powerful magic of the fire ability can be viewed in /Assets/Scripts/Weapons/FireWeapon.cs

happy gaming :)